:-module(query_utility,[
	query_print_result/1,
	query_print_time/1,
	query_print_time_result/1,
	query_print_time_result/3]).



% utility
query_print_result(OutFilePath) :-
	prob(run,A,B),
	print_results(OutFilePath,A,B).

query_print_time(OutFilePath) :-
	S is cputime,
	prob(run,A,B),
	E is cputime,
	T is E-S,
	print_time(OutFilePath,A,B,T).

query_print_time_result(OutFilePath) :-
	S is cputime,
	prob(run,A,B),
	E is cputime,
	T is E-S,
	print_results(OutFilePath,A,B,T).

query_print_time_result(OutFilePath,A,B) :-
	S is cputime,
	prob(run,A,B),
	E is cputime,
	T is E-S,
	print_results(OutFilePath,A,B,T).

% test :-
% 	S is cputime,
% 	prob(run,A,B),
% 	E is cputime,
% 	T is E-S,
% 	open('results_time.txt',write,OS),
% 	format(OS,"Time: ~f~n~n===============~n~n",[T]),
% 	close(OS).

print_results(OutFilePath,A,B):-
	append_path(OutFilePath,["results.txt"],[OutFile]),
	open(OutFile,write,OS),
	length(A,N),
	format(OS,"Statistics~nN. Worlds: ~d~nProb: ~f~n~n- WORLDS~n===============~n~n",[N,B]),
	print_w(A,OS),
	close(OS).

print_results(OutFilePath,A,B,T):-
	append_path(OutFilePath,["results_time.txt"],[OutFile]),
	open(OutFile,write,OS),
	length(A,N),
	format(OS,"Statistics~nN. Worlds: ~d~nProb: ~f~nTime: ~f~n~n- WORLDS~n===============~n~n",[N,B,T]),
	print_w(A,OS),
	close(OS).

print_w([],_).
print_w([world(ICS,_,P)|T],OS) :-
	print_ics(ICS,OS),
	format(OS,"Prob: ~f~n~n===============~n~n",[P]),
	print_w(T,OS).


print_ics([],_).
print_ics([IC|T],OS) :-
	writeln(OS,IC),
	print_ics(T,OS).

print_time(OutFilePath,A,B,T):-
	append_path(OutFilePath,["time.txt"],[OutFile]),
	open(OutFile,write,OS),
	length(A,N),
	format(OS,"Statistics~nN. Worlds: ~d~nProb: ~f~nTime: ~f~n~n===============~n~n",[N,B,T]),
	close(OS).