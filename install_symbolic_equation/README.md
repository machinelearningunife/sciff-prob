## Installation
Do the following:

- Make sure you have installed `SWI-Prolog` and updated: see https://www.swi-prolog.org/build/unix.html
- Make sure you have installed the `SWI-Prolog` package `bddem`:
```
swipl
?- pack_install(bddem).
```
(github download is suggested)
- Make sure you have installed the library `nlopt` on your machine (for example by installing the Ubuntu package `libnlopt-dev`)
- Now, go to the directory where `bddem` is stored (it should be in `/home/<user>/.local/share/swi-prolog/pack`). 
    - Replace `bddem/prolog/bddem.pl` with the file `bddem.pl` in this folder
    - Replace `bddem/bddem.c` with the file `bddem.c` in this folder
- Rebuild `bddem` with
``` 
swipl
?- pack_rebuild(bddem).
```
- Everything should be ready now. If it does not compile, delete folder `bddem/lib` and re-run the command `pack_rebuild/1`.

## Remove Symbolic equation
Comment out lines 2429, 2505, 2513 of `sciff.pl`. In this way, you can use the version of `bddem` from the original git, but you won't have the creation of the symbolic equation.