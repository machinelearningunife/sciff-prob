:- module(test_sciff_config,
  [test/0]).
:- use_module(library(plunit)).


:- format(user_error,
	  'SCIFF test suite.  To run all tests run ?- test.~n~n', []).

epsilon(0.09).

test:-
  run_tests([
	  probChristiansen,
	  murder,
	  probNatureLover]).


close_to(V,T):-
	epsilon(E),
	TLow is T-E,
	THigh is T+E,
	TLow<V,
	V<THigh.

close_to(V,T,E):-
	TLow is T-E,
	THigh is T+E,
	TLow<V,
	V<THigh.

same_worlds(Expl, CorrExpl):-
	same_number_of_worlds(Expl, CorrExpl),
	same_worlds_int(Expl, CorrExpl).

same_number_of_worlds(Expl, CorrExpl):-
	length(Expl,NE),
	length(CorrExpl,NE).

same_worlds_int(CorrExpl, CorrExpl) :- !.

same_worlds_int([],_CorrExpls) :- !.

same_worlds_int([world(Expl,_,Prob)|Expls],CorrExpls):-
  member(world(ExplX,_,ProbX),CorrExpls),
  sort(Expl,ExplSort),
  sort(ExplX,ExplSort),
  close_to(Prob,ProbX),!,
  same_worlds_int(Expls,CorrExpls).


:- begin_tests(probChristiansen,[]).

test(pcq) :-
    project(probChristiansen),
    query_print_time_result('./experiments/probChristiansen/',Worlds,Prob),
	close_to(Prob,0.199695),
	length(Worlds,1600).

:- end_tests(probChristiansen).


:- begin_tests(murder,[]).

test(mq) :-
    project(murder),
    query_print_time_result('./experiments/murder/',Worlds,Prob),
	close_to(Prob,1.000000),
	same_worlds(Worlds,[
		world([ics(added,[abd(enter(husband,house(father(husband))))],[[has_keys(husband,house(father(husband)))]],1,0.7),ics(added,[abd(enter(husband,house(husband)))],[[has_keys(husband,house(husband))]],1,0.7)],[],0.48999999999999994),
		world([ics(removed,[abd(enter(husband,house(father(husband))))],[[has_keys(husband,house(father(husband)))]],1,0.7),ics(added,[abd(enter(husband,house(husband)))],[[has_keys(husband,house(husband))]],1,0.7)],[],0.21000000000000002),
		world([ics(added,[abd(enter(husband,house(father(husband))))],[[has_keys(husband,house(father(husband)))]],1,0.7),ics(removed,[abd(enter(husband,house(husband)))],[[has_keys(husband,house(husband))]],1,0.7)],[],0.21000000000000002),
		world([ics(removed,[abd(enter(husband,house(father(husband))))],[[has_keys(husband,house(father(husband)))]],1,0.7),ics(removed,[abd(enter(husband,house(husband)))],[[has_keys(husband,house(husband))]],1,0.7)],[],0.09000000000000002)
	]).

:- end_tests(murder).

:- begin_tests(probNatureLover,[]).

test(pnlq) :-
    project(probNatureLover),
    query_print_time_result('./experiments/probNatureLover/',Worlds,Prob),
	close_to(Prob,0.100000),
	same_worlds(Worlds,[world([ics(removed,[abd(pet(dino))],[],3,0.9)],[],0.09999999999999998)]).

:- end_tests(probNatureLover).