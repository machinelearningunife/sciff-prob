accepted_customer(Customer) :- resident_in(Customer, Zone), accepted_dest(Zone).
rejected_customer(Customer) :- resident_in(Customer, Zone), not(accepted_dest(Zone)).
accepted_payment(cc).
accepted_payment(cash).
accepted_dest(europe).