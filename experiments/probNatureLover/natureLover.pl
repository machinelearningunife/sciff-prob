

society_goal:- natureLover(fred).
%society_goal:- natureLover(kevin).

% a nature lover is someone that has at least an animal which is a pet
natureLover(X) :- has_animal(X,Y), abd(pet(Y)).

% kevin and fred have animals: kevin has fluffy and tom, while fred has dino.
has_animal(kevin,fluffy).
has_animal(kevin,tom).
has_animal(fred,dino).

% here what animals are fluffy, tom, and dino
cat(fluffy).
cat(tom).
dinosaur(dino).

%0.2 :: dog(X), abd(pet(X)) ---> false.
%0.4 :: cat(X), abd(pet(X)) ---> false.
%0.9 :: dinosaur(X), abd(pet(X)) ---> false. 
