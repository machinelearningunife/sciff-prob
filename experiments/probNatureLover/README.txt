
In this project we model that every one who has a pet animal is a nature lover and that kevin has two animals, the cats fluffy and tom, while fred has as animal a dinosaur called dino.

We have three constraints telling that a dog is not a pet with probability 0.2 (so, a dog is a pet with probability 0.8), a cat is not a pet with probability 0.4 and a dinosaur is not a pet with probability 0.9.

pet is an abducible.

If the goal (defined using society_goal) is if fred is a nature lover, we can find only one world in which the third constraint is removed. The final probability is 0.1.

If the goal is if kevin is a nature lover we have two worlds, one where the first contraint is removed and one where the second contraint is removed. The final probability is 0.84.