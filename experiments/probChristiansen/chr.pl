
% The network structure
edge(w1, pp, n1).
edge(w4, n3, v3).
edge(w7, n3, v2).
edge(w2, n1, n2).
edge(w5, n1, n4).
edge(w8, n4, v4).
edge(w3, n2, n3).
edge(w6, n2, v1).
edge(w9, n4, v5).

% The fact that a given point in the network has electricity, is described as follows.
haspower(pp):- abd(up(pp)).
haspower(N2):- edge(W,N1,N2), abd(up(W)), haspower(N1). 

%As no negation is supported, the program includes also clauses that simulate the
%negation of haspower.
hasnopower(pp):- abd(down(pp)).
hasnopower(N2):- edge(W,_,N2), abd(down(W)).
hasnopower(N2):- edge(_,N1,N2), hasnopower(N1).


%Query
society_goal:- hasnopower(v1),
			   hasnopower(v2),
			   hasnopower(v3),
			   hasnopower(v4),
			   hasnopower(v5).



