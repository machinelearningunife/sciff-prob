
society_goal:- killer(oj).  % calculate the probability that OJ Simpson is the killer
killer(X) :- abd(killed(X,Y)). % X is a killer if he has killed a person Y

was_killed(nicole).
was_killed(ronald).

relationship(oj,nicole,ex_wife).
relationship(oj,ronald,friend).

