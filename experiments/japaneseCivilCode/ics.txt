% We introduce the \NULL\ abducible to mean that an expectation is
% nullified; semantically, we model this by saying that a nullified
% expectation is always fulfilled, regardless of the presence in the
% history of a fulfilling event.

%e(Content,T_n) /\ abd(null(Content),_) ---> discharge(e(Content,T_n)).

% MG: La precedente non funziona nella implementazione attuale della SCIFF, perche' non si puo` estrarre
% il functor di e(Content,_), perche' Content e` una variabile in questo IC.

% we can express that the expectation raised by a
% manifestation of intention that is explicitly permitted to be
% rescinded can be nullified, by performing the rescission.

%    !en(rescind(A,I,F,IdI,IdR)) /\
%    h(rescind(A,I,F,IdI,IdR)) ---> abd(null(do(A,I))).



!en(rescind(A,B,I,F,IdI,IdR)) /\ h(rescind(A,B,I,F,IdI,IdR))
  ---> abd(null(IdI,IdR)).


abd(null(IdI,_IdR)) /\ e(do(A,I,IdI)) ---> discharge(e(do(A,I,IdI))).

% A manifestation of intention should, in general, be followed by the performing of the act.

  h(intention(A,_B,I,IdI)) ---> e(do(A,I,IdI)).


% Manifestation of intention which is induced by any fraud or duress may be rescinded.

    h(intention(A,B,I,IdI)) 
    /\ h(do(B,F,IdI))
    /\ h(induce(F,I)) 
    /\ fraudOrDuress(F)
     ---> !en(rescind(A,B,I,F,IdI,_IdR)).

% In cases any third party commits any fraud inducing any person to make a
% manifestation of intention to the other party, such manifestation of intention may be
% rescinded only if the other party knew such fact.

    h(intention(A,B,I,IdI)) 
    /\ h(do(C,F,IdI)) /\ C != B
    /\ h(know(B,F))
    /\ h(induce(F,I)) 
    /\ fraudOrDuress(F)    
    ---> !en(rescind(A,B,I,F,IdI,_IdR)).

%  The rescission of the manifestation of intention induced by the fraud pursuant to
%  the provision of the preceding two paragraphs may not be asserted against a third
%  party without knowledge.

      h(rescind(A,_B,_I,F,_IdI,IdR))
      /\ h(not_know(C,F)) 
%MG: QUESTO CAUSA PROBLEMI:              /\ C != B
      --->en(assertAgainst(A,C,IdR)).



% work on an acquired good is to be paid by the
%  good's owner, unless owner rescinds the good's purchase and asserts
%  the rescission against performer of the work 

    h(work(M,G,O,W))
    --->e(pay(O,M,W))
    \/e(rescind(O,_B,buy(G),_F,_IdI,IdR))
    /\e(assertAgainst(O,M,IdR)).


