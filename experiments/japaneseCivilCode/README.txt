Per Jurisin16:
Cerchiamo di mappare l'art 96 del codice civile giapponese:

(Fraud or Duress)
Article 96 (1) Manifestation of intention which is induced by any fraud or duress may be rescinded.
(2) In cases any third party commits any fraud inducing any person to make a 
manifestation of intention to the other party, such manifestation of intention may be 
rescinded only if the other party knew such fact.
(3) The rescission of the manifestation of intention induced by the fraud pursuant to 
the provision of the preceding two paragraphs may not be asserted against a third 
party without knowledge.

Inseriamo nuovi meta-IC che permettono di rendere fulfilled le obbligazioni anche senza il matching event, ad esempio perche' il contratto e` stato annullato.


