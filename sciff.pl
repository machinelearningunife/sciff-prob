% Probabilistic version by Marco Gavanelli, 2020

%----------------------------------------------------------
% "sciff.pl"
%
% Initial Author: Marco Alberti
% Created: Sept. 29, 2003
% with modifications by Marco Gavanelli and Federico Chesani
%----------------------------------------------------------
%:- multifile(fdet/1).
%:- dynamic(fdet/1).

%:- module(sciff,[prob/3, run/0, run/2, run_no_close/0, run_no_close/2]).

%----------------------------------------------------------
% CONSULTING THE MODULES
%----------------------------------------------------------




:- use_module(library(terms)).
%:- [my_chr_extensions]. % In SWI, my_chr_extensions must be loaded after reified_unif
:- use_module(my_chr_extensions).
:- use_module(reified_unif).

:- use_module(prolog_version).
:- (is_dialect(swi) 
	-> use_module(swi_specific),
       [my_chr_extensions] % In SWI, my_chr_extensions must be loaded after reified_unif
	 ;
    is_dialect(sicstus)
    -> ensure_loaded(my_chr_extensions),
       use_module(sicstus_specific)
    ;   write('Unrecognized Prolog System'), nl, fail
    ).

:- use_module(library(chr)).
:- use_module(proof_util).
:- use_module(quantif).
:- use_module(ccopy).
:- use_module(ics_quant).
:- use_module(library(lists)).
%         [append/3,
%          delete/3,
%      		nth/4,
%      		member/2]).
%:- use_module(library(terms),         [term_variables/2]).

%:- ensure_loaded(solver).
:- use_module(solver).
%:- use_module(domains).
:- use_module(history_parser).
:- use_module(sokb_parser).
:- use_module(ics_parser).
:- use_module(sciff_options).
:- use_module(debug).
:- use_module(help).
:- use_module(graphviz).

:- (exists_file('defaults.pl')
	-> ensure_loaded(defaults)
	; write('WARNING: File defaults.pl not found. Assuming default dir is ..'), nl,
	dynamic(default_dir/1),
	assert(default_dir('../'))
	).
:- use_module(library(clpfd)).  % used by invert_constraint in SWI, and in old SCIFF code (e.g., block world)
:- [pretty_print].

:- reexport(library(bddem)).

:- use_module(query_utility).

%----------------------------------------------------------
% DECLARATIONS
%----------------------------------------------------------
%handler society_proof.

%option(already_in_store,on).

:- chr_option(debug,on). % Se lo metto on mi da` un errore




%----------------------------------------------------------
% IMPOSING IC/PSIC
%
% Quantify the variables in an imposed IC, and impose the result as a
% PSIC
%----------------------------------------------------------
:- chr_constraint
	ic/4, psic/3.
impose_ics @
    ic(Body1,Head1,P,ID)
    ==>
    %unfold_nonrecursive(psic(Body1,Head1),PSIC),
    %impose_psic(PSIC).
    convert_to_psic(ic(Body1,Head1,P,ID),PSIC),
    call(PSIC).
    %psic(BodyPSIC,HeadPSIC).

%convert_to_psic: converts an IC to a PSIC.
% BodyIn/HeadIn are useful because this predicate is also used in unfolding
convert_to_psic(ic(Body1,Head1,P,ID),PSIC):- convert_to_psic(ic(Body1,Head1,P,ID),[],[],PSIC).
convert_to_psic(ic(Body1,Head1,P,ID),BodyIn,HeadIn,PSIC):-
    unfold_nonrecursive(psic(Body1,Head1),NewPSIC),
    (NewPSIC = psic(BodyUnf,HeadUnf)
        ->  quantify_variables_in_ics(ics(BodyUnf,HeadUnf),
                  ics(Body2,Head2)),
            rewrite_body_terms(Body2,Body3),
            BodyPSIC=Body3, HeadPSIC=Head2,
            PSIC=psic(BodyPSIC,HeadPSIC,ics(BodyIn,HeadIn,ID,P))
        ;   PSIC=true).


%%MG: se aggiungo un argomento a psic che e` ics(Body2,Head2)? Dovrei ritrovarmelo ground (qualora diventi ground lo psic).
%% NO, perche' in tutte le propagation faccio una copia dell'IC, quindi se metto solo ics(Body2,Head2) mi ritrovo qualcosa di non ground.
%% A questo punto, la cosa va gestita nelle propagation, quindi qui metto solo un placeholder e nelle propagation aggiungo gli atomi uno alla volta.

%----------------------------------------------------------
% IMPOSING VARIOUS CONSTRAINTS
%----------------------------------------------------------
%:- chr_type functarity ---> [any,int].
%:- chr_type posexp ---> e(functarity,?,int).
%:- chr_type list(T) --->    [] ; [T | list(T)].
:- chr_constraint
    h/2, en/2, note/1, noten/1, fulf/1, viol/1, pending/1, abd/2,
    e/2.
    %e(+functarity,?,?int).
    %ground_time/0.

get_functor(do(_,_,Main,_),[F,A]):- !, functor(Main,F,A).
get_functor(do(_,_,_,Main,_),[F,A]):- !, functor(Main,F,A).
get_functor(Main,[F,A]):- !, functor(Main,F,A).

h(Event):-
    get_functor(Event,F),
    h(F,Event).

e(Event):-
    get_functor(Event,F),
    e(F,Event).

en(Event):-
    get_functor(Event,F),
    en(F,Event).

abd(Event):-
    get_functor(Event,F),
    abd(F,Event).

% In SICStus 4 there is no remove_constraint, so we remove constraints
% with simpagation rules
fulf(e(F,EEvent)) \ pending(e(F,EEvent)) <=> true.
viol(en(F,EEvent)) \ pending(en(F,EEvent)) <=> true.
viol(e(F,EEvent)) \ pending(e(F,EEvent)) <=> true.  
viol(gt_current_time(e(F,Event), _)) \ pending(e(F,Event)) <=> true.

fulf(e(F,FEvent)), pending(e(F,PEvent)) ==>
	sciff_option(nonallowed_discharge,on), fn_ok(FEvent,PEvent) |
	(p(FEvent)=p(PEvent) ;
	reif_unify(p(FEvent),p(PEvent),0)).
%	(nonvar(Bool)
%	 -> nondeterministic((Bool=1;Bool=0))
%	  ; true).

% Adds the [functor,arity] term to expectations & happened events
% in the body of ICs.
rewrite_body_terms([H,NotH,E,NotE,En,NotEn,Abd,A],[H1,NotH,E1,NotE,En1,NotEn,Abd1,A]):-
    rewrite_body_atom(H,H1),
    rewrite_body_atom(E,E1),
    rewrite_body_atom(Abd,Abd1),
    rewrite_body_atom(En,En1).
    
rewrite_body_atom([],[]).
rewrite_body_atom([h(E)|LH],[h(F,E)|LH1]):-
    get_functor(E,F),
    rewrite_body_atom(LH,LH1).
rewrite_body_atom([e(E)|LH],[e(F,E)|LH1]):-
    get_functor(E,F),
    rewrite_body_atom(LH,LH1).
rewrite_body_atom([en(E)|LH],[en(F,E)|LH1]):-
    get_functor(E,F),
    rewrite_body_atom(LH,LH1).
rewrite_body_atom([abd(E)|LH],[abd(F,E)|LH1]):-
    get_functor(E,F),
    rewrite_body_atom(LH,LH1).







%----------------------------------------------------------
% EVENT CALCULUS and FACTORING
%
% To carefully think about it...
%----------------------------------------------------------
% Questo andrebbe messo come IC.
% Non posso avere 2 eventi nello stesso tempo
% sequential_act @
%     e([act,1],act(E1),T1) \
%     e([act,1],act(E2),T2)
%     <=> sciff_option(seq_act,on),
%     T2=T1 | E1=E2.


% factoring
% This factoring is specilised for the abductive event calculus
% Questo factoring va legato ad una regola che toglie uno degli abd
% se ce ne sono 2 identici. Nel caso dell'EC c'e` gia` (che vieta 2 eventi
% nello stesso tempo).

%factoring_ec @
%    e(act(X1),T1), e(act(X2),T2)
%    ==>
%    sciff_option(factoring,on),
%    nonvar(X1), nonvar(X2) |
%    reif_unify(e(act(X1),T1),e(act(X2),T2),B),
%    (B=1 ; B=0).


%% Secondo tentativo: fa l'unificazione dei tempi solo se l'azione
%% e` gia` ground
% factoring2 @
%     e(F,X,T1), e(F,X,T2)
%     ==>
%     sciff_option(factoring,on),
%     ground(X) |
%     (eq(T1,T2) ;
%     neq(T1,T2), when(?=(T1,T2),T1\==T2)).
% La seconda serve perche' cosi` se e` la stessa var fallisce
% senza dover usare la (poca) propagazione del diverso.
%%    T1 #= T2 #<=> B,
%%    (B=1 ; B=0).

remove_duplicate_abd @
    abd(F,X) \ abd(F,X) <=> true.

factoring_abd @
    abd(F,X1), abd(F,X2)
    ==>
    sciff_option(factoring,on) |
    reif_unify(abd(X1),abd(X2),B),
    (B=1 ; B=0).


%----------------------------------------------------------
% ITERATIVE DEEPENING: max number of e(act(...)).
%----------------------------------------------------------
:- chr_constraint 
	max_depth/1.
max_depth_e_act @
e([act,1],act(_)), max_depth(DepthMax) ==>
    %findall_constraints(e(_,act(_),_),L), length(L,N), N>DepthMax | fail.
    max_constraints(e(_,act(_)),DepthMax).


%----------------------------------------------------------
% ITERATIVE DEEPENING: max number of violation
%----------------------------------------------------------
:- chr_constraint
	max_viol/1.
max_violations @
viol(_), max_viol(MaxViol) ==> 
    findall_constraints(viol(_),L), %write(L), 
    length(L,N), 
    write(N) 
    | 
    lt(MaxViol,1000), 
    leq(N,MaxViol).





e_consistency @
    e(F,EEvent),
    en(F,ENEvent)
    ==>
    %findall_constraints(gen_phase,[]) |
    %add_arc_graph('E-consistency',e(EEvent,ETime)\=en(ENEvent,ENTime)),
    check_unification(p(EEvent),p(ENEvent),0,e,en).



%
pending_en @
    en(F,Event)
    ==>
    pending(en(F,Event)).


:- chr_constraint
	nondeterministic/1, phase/1, remove_phase/0.

propagation_h @
    h(F,Event1),
    psic([[h(F,Event2)|MoreH],NotH,E,NotE,En,NotEn,Abd,A],Head,ics(GroundBody,GroundHead,IC_ID,P))
    ==>
    fn_ok(Event1,Event2)
    |
    ccopy(p([[h(Event2)|MoreH],NotH,E,NotE,En,NotEn,Abd,A],Head),
      p([[h(Event2a)|MoreHa],NotHa,Ea,NotEa,Ena,NotEna,Abda,Aa],Heada)),
    (subsumeschk(p(Event2a),p(Event1)),
     is_term_quantified(h(Event2a),forall)
     ->    %reif_unify(p(Event1,Time1),p(Event2a,Time2a),1),
            (Event1=Event2a
                ->  psic([MoreHa,NotHa,Ea,NotEa,Ena,NotEna,Abda,Aa],Heada,ics([h(Event1)|GroundBody],GroundHead,IC_ID,P))
                ;   true)
     ;  reif_unify(p(Event1),p(Event2a),B),
        PSIC=psic([MoreHa,NotHa,Ea,NotEa,Ena,NotEna,Abda,Aa],Heada,ics([h(Event1)|GroundBody],GroundHead,IC_ID,P)),
        (B==1 -> call(PSIC) ;
         B==0 -> true ;
        nondeterministic((B=1, call(PSIC);
        B=0))
        )
    ).

propagation_e @
    e(F,Event1),
    psic([[],NotH,[e(F,Event2)|MoreE],NotE,En,NotEn,Abd,A],Head,ics(GroundBody,GroundHead,IC_ID,P))
    ==>
    fn_ok(Event1,Event2)
    |
    ccopy(p([[],NotH,[e(Event2)|MoreE],NotE,En,NotEn,Abd,A],Head),
      p([[],NotHa,[e(Event2a)|MoreEa],NotEa,Ena,NotEna,Abda,Aa],Heada)),
   reif_unify(p(Event1),p(Event2a),B),
   status(S,F),
       (draw_graph(S,propagation_e,e(Event1)=e(Event2)),
       B=1, psic([[],NotHa,MoreEa,NotEa,Ena,NotEna,Abda,Aa],Heada,ics([e(Event1)|GroundBody],GroundHead,IC_ID,P))
       ;
       draw_graph(S,propagation_e,e(Event1)\=e(Event2)),
       B=0).
% End modification
% Original version:
/*
propagation_e @
    e(Event1,Time1),
    psic([H,NotH,[e(Event2,Time2)|MoreE],NotE,En,NotEn,Abd,A],Head)
    ==>
    fn_ok(Event1,Event2)
    |
    ccopy(p([H,NotH,[e(Event2,Time2)|MoreE],NotE,En,NotEn,Abd,A],Head),
      p([Ha,NotHa,[e(Event2a,Time2a)|MoreEa],NotEa,Ena,NotEna,Abda,Aa],Heada)),
   reif_unify(p(Event1,Time1),p(Event2a,Time2a),B),
   status(S,F),
       (draw_graph(S,propagation_e,e(Event1,Time1)=e(Event2,Time2)),
       B#=1, psic([Ha,NotHa,MoreEa,NotEa,Ena,NotEna,Abda,Aa],Heada)
       ;
       draw_graph(S,propagation_e,e(Event1,Time1)\=e(Event2,Time2)),
       B#=0).
*/
% End Original version

propagation_note @
    note(Event1),
    psic([[],NotH,[],[note(Event2)|MoreNotE],En,NotEn,Abd,A],Head,ics(GroundBody,GroundHead,IC_ID,P))
    ==>
    fn_ok(Event1,Event2)
    |
    ccopy(p([[],NotH,[],[note(Event2)|MoreNotE],En,NotEn,Abd,A],Head),
          p([[],NotHa,[],[note(Event2a)|MoreNotEa],Ena,NotEna,Abda,Aa],Heada)),
   reif_unify(p(Event1),p(Event2a),B),
       (B=1, psic([[],NotHa,[],MoreNotEa,Ena,NotEna,Abda,Aa],Heada,ics([note(Event1)|GroundBody],GroundHead,IC_ID,P));
       B=0).
% End Modification       
% Original Version:
/*
propagation_note @
    note(Event1,Time1),
    psic([[],NotH,[],[note(Event2,Time2)|MoreNotE],En,NotEn,Abd,A],Head)
    ==>
    fn_ok(Event1,Event2)
    |
    ccopy(p([H,NotH,E,[note(Event2,Time2)|MoreNotE],En,NotEn,Abd,A],Head),
    
p([Ha,NotHa,Ea,[note(Event2a,Time2a)|MoreNotEa],Ena,NotEna,Abda,Aa],Heada)),
   reif_unify(p(Event1,Time1),p(Event2a,Time2a),B),
       (B#=1, psic([Ha,NotHa,Ea,MoreNotEa,Ena,NotEna,Abda,Aa],Heada);
       B#=0).
*/

/*
propagation_e @
    e(F,Event1,Time1),
    psic([[],NotH,[e(F,Event2,Time2)|MoreE],NotE,En,NotEn,Abd,A],Head)
    ==>
    fn_ok(Event1,Event2)
    |
    ccopy(p([[],NotH,[e(Event2,Time2)|MoreE],NotE,En,NotEn,Abd,A],Head),
      p([[],NotHa,[e(Event2a,Time2a)|MoreEa],NotEa,Ena,NotEna,Abda,Aa],Heada)),
   reif_unify(p(Event1,Time1),p(Event2a,Time2a),B),
   status(S,F),
       (draw_graph(S,propagation_e,e(Event1,Time1)=e(Event2,Time2)),
       B#=1, psic([[],NotHa,MoreEa,NotEa,Ena,NotEna,Abda,Aa],Heada)
       ;
       draw_graph(S,propagation_e,e(Event1,Time1)\=e(Event2,Time2)),
       B#=0).
*/

propagation_en @
    en(F,Event1),
    psic([[],NotH,[],[],[en(F,Event2)|MoreEn],NotEn,Abd,A],Head,ics(GroundBody,GroundHead,IC_ID,P))
    ==>
    fn_ok(Event1,Event2)
    |
    ccopy(p([[],NotH,[],[],[en(Event2)|MoreEn],NotEn,Abd,A],Head),
      p([[],NotHa,[],[],[en(Event2a)|MoreEna],NotEna,Abda,Aa],Heada)),
    ccopy( p(  Event1),
           p( Event1a)),
     reif_unify(p(Event1a),p(Event2a),B),
       (B=1, psic([[],NotHa,[],[],MoreEna,NotEna,Abda,Aa],Heada,ics([en(Event1a)|GroundBody],GroundHead,IC_ID,P));
       B=0).

propagation_noten @
    noten(Event1),
    psic([[],NotH,[],[],[],[noten(Event2)|MoreNotEn],Abd,A],Head,ics(GroundBody,GroundHead,IC_ID,P))
    ==>
    fn_ok(Event1,Event2)
    |
    ccopy( p([[], NotH, [], [], [], [noten(Event2)| MoreNotEn], Abd,A], Head),
           p([[],NotHa,[],[],[],[noten(Event2a)|MoreNotEna],Abda,Aa],Heada)),
    ccopy( p(  Event1),
           p( Event1a)),
	reif_unify(p(Event1a),p(Event2a),B),
    (	B=1, psic([[],NotHa,[],[],[],MoreNotEna,Abda,Aa],Heada,ics([noten(Event1a)|GroundBody],GroundHead,IC_ID,P))
    	;
       	B=0).
  

propagation_abd @
    abd(F,Event1),
    psic([[],NotH,[],[],[],[],[abd(F,Event2)|MoreAbd],A],Head,ics(GroundBody,GroundHead,IC_ID,P))
    ==>
    fn_ok(Event1,Event2)
    |
    %ccopy(p([[],NotH,[],[],[],[],[abd(Event2,Time2)|MoreAbd],A],Head),
	%				p([[],NotHa,[],[],[],[],[abd(Event2a,Time2a)|MoreAbda],Aa],Heada)),
	ccopy(p(NotH, Event2, MoreAbd, A, Head),
		  p(NotHa,Event2a,MoreAbda,Aa,Heada)),
    (subsumeschk(p(Event2a),p(Event1)),
     is_term_quantified(abd(Event2a),forall)
     -> (Event2a = Event1
            ->  psic([[],NotHa,[],[],[],[],MoreAbda,Aa],Heada,ics([abd(Event1)|GroundBody],GroundHead,IC_ID,P))
            ;   true)
     ;  reif_unify(p(Event1),p(Event2a),B),
        (B=1, psic([[],NotHa,[],[],[],[],MoreAbda,Aa],Heada,ics([abd(Event1)|GroundBody],GroundHead,IC_ID,P));
        B=0)
    ).





propagation_violated @
    viol(Event1),
    psic([[],NotH,[],[],[],[],[abd([viol,1],viol(Event2))|MoreAbd],A],Head,ics(GroundBody,GroundHead,IC_ID,P))
    ==>
    sciff_option(violation_causes_failure,no),
    fn_ok(Event1,Event2)
    |
    ccopy(p([[],NotH ,[],[],[],[],[viol(Event2) |MoreAbd], A ],Head),
          p([[],NotHa,[],[],[],[],[viol(Event2a)|MoreAbda],Aa],Heada)),
    reif_unify(p(Event1),p(Event2a),B),
%       (B#=1, psic([[],NotHa,[],NotEa,ENa,NotEna,MoreAbda,Aa],Heada);
       (B=1, psic([[],NotHa,[],[],[],[],MoreAbda,Aa],Heada,ics([viol(Event1)|GroundBody],GroundHead,IC_ID,P));
       B=0).

% Two terms may be unifiable.
% Profiled version, nearly twice as fast
fn_ok(Term1,Term2):- Term1 == Term2,!. % either if they are identical
fn_ok(Term1,Term2):- \+(?=(Term1,Term2)). % or if they are syntactically unifiable

/* old version
fn_ok(Term1,Term2):-
    nonvar(Term1),
    nonvar(Term2),
    !,
    Term1=..[H|T1],
    Term2=..[H|T2],
    fn_ok_list(T1,T2).
fn_ok(_,_).

fn_ok_list([],[]).
fn_ok_list([H1|T1],[H2|T2]):-
    fn_ok(H1,H2),
    fn_ok_list(T1,T2).
*/

split_list(List,1,[],List):-
    !.
split_list([Head|Tail],N,[Head|Tail1],Rest):-
    N1 is N-1,
    split_list(Tail,N1,Tail1,Rest).

type_position(h,1).
type_position(noth,2).
type_position(e,3).
type_position(note,4).
type_position(en,5).
type_position(noten,6).
  


logical_equivalence @
    psic([[],[],[],[],[],[],[],[]],Head,ics(_,_,_,1))
    <=>
    add_arc_graph(logical_equivalence,(true->Head)),
    impose_head1(Head).

:- chr_constraint world_prob/2.
/*
Qui forse dobbiamo aggiungere un nuovo world solo se l'IC non c'e` gia`.
Non solo: dobbiamo anche imporre la disunificazione di cio` che c'e` nel body
Forse potremmo imporre la reified unification ed aprire due strade: se unifica, non crei un nuovo mondo; se disunifica si`.
*/

prob_granularity(1000). % Probabilities are integers with denominator this number

logical_equivalence_prob @
    psic([[],[],[],[],[],[],[],[]],Head,ics(OrigBody,OrigHead,IC_ID,P)),
    world_prob(W,WP) # passive
    <=>
    matches_one_world_elem(ics(OrigBody,OrigHead,IC_ID,P),W,DoesMatch,Bools),
    prob_granularity(Granularity),
%    write_error(doesMatch(DoesMatch,Bools)),
    (   DoesMatch==1 -> world_prob(W,WP) ;
        (   DoesMatch=1, world_prob(W,WP),
            label_bools_one_true(Bools)
        ;
            DoesMatch=0,
            (  
                add_arc_graph(logical_equivalence,(true->Head)),
                WP1 #= P*WP//Granularity,     new_world(W,ics(added,OrigBody,Head,IC_ID,P),WP1),
                impose_head1(Head)
            ;   WP1 #= (Granularity-P)*WP//Granularity, new_world(W,ics(removed,OrigBody,Head,IC_ID,P),WP1)
            )
        )
    ).

/*
logical_equivalence_prob @
    psic([[],[],[],[],[],[],[],[]],Head,ics(OrigBody,OrigHead,IC_ID,P)),
    world_prob(W,WP) # passive
    <=>
    matches_one_world_elem(ics(OrigBody,OrigHead,IC_ID,P),W,DoesMatch,Bools),
    write_error(doesMatch(DoesMatch,Bools)),
    (   DoesMatch==1 -> world_prob(W,WP) ;
        (   DoesMatch=1, world_prob(W,WP),
            label_bools_one_true(Bools)
        ;
            DoesMatch=0,
            (  
                add_arc_graph(logical_equivalence,(true->Head)),
                WP1 is P*WP,     new_world(W,ics(added,OrigBody,Head,IC_ID,P),WP1),
                impose_head1(Head)
            ;   WP1 is (1-P)*WP, new_world(W,ics(removed,OrigBody,Head,IC_ID,P),WP1)
            )
        )
    ).
*/


/*
% VECCHIA VERSIONE:
 logical_equivalence_prob @
    psic([[],[],[],[],[],[],[],[]],Head,ics(OrigBody,_OrigHead,IC_ID,P)),
    world_prob(W,WP) # passive
     <=>
    (   
        add_arc_graph(logical_equivalence,(true->Head)),
        WP1 is P*WP,     new_world(W,ics(added,OrigBody,Head,IC_ID,P),WP1),
        impose_head1(Head)
    ;   WP1 is (1-P)*WP, new_world(W,ics(removed,OrigBody,Head,IC_ID,P),WP1)
    ).
*/

matches_one_world_elem(_,[],0,[]):- !.
matches_one_world_elem(ics(OrigBody,OrigHead,IC_ID,P),[ics(_AddedOrRemoved,Body,_Head,IC_ID,P)|W],DoesMatch,Bools):- !, % Same ID and prob
%    write_error(before_reif_unify(OrigBody,Body,Unifies)),nl,
    reif_unify(OrigBody,Body,Unifies),
%    write_error(after_reif_unify(OrigBody,Body,Unifies)),nl,
    (Unifies == 1 -> DoesMatch = 1, Bools=[]  ; % Impongo che anche le head unifichino: se e` lo stesso ic con lo stesso body ...
     (Unifies == 0 -> matches_one_world_elem(ics(OrigBody,OrigHead,IC_ID,P),W,DoesMatch,Bools)
        ; matches_one_world_elem(ics(OrigBody,OrigHead,IC_ID,P),W,DoesMatch1,Bools1),
          (DoesMatch1 == 1 -> DoesMatch = 1, Bools1=[];
           (DoesMatch1 == 0 -> DoesMatch = Unifies, Bools=Bools1
             ; chr_or(DoesMatch1,Unifies,DoesMatch), Bools = [Unifies|Bools1]
           )
          )
     )
    ).
matches_one_world_elem(ics(OrigBody,OrigHead,IC_ID,P),[_|W],DoesMatch,Bools):-  % different ID or prob
    matches_one_world_elem(ics(OrigBody,OrigHead,IC_ID,P),W,DoesMatch,Bools).

debugging @
    psic([[],[],[],[],[],[],[],[]],Head,P)
    ==>
        write("ERRORE: generato un IC: true ---> "),
        write(Head),
        write(" con struttura dati del mondo "),
        writeln(P).

%:- dynamic world_id/1.
%:- dynamic mystop/0.

%world_id(1).

init_world:-
    prob_granularity(Granularity),
    world_prob([],Granularity).
%    retractall(world_id(_)),
%    assert(world_id(1)).

new_world(W,ICS,WP):-
%    retract(world_id(ID)),    ID1 is ID+1,    assert(world_id(ID1)),
  % text_property(bold,1), writeln(new_world_id(ID1)), reset_text_style,
    world_prob([ICS|W],WP).

%print_world_id:-
%    world_id(ID),
%    foreground_color(red), text_property(bold,1), writeln(world_id(ID)), reset_text_style.
%    (assert(mystop) ; retract(mystop)).

:- chr_constraint query_world/2.

world_prob(A,B) \ query_world(X,Y) <=> A=X, B=Y.


label_bools_one_true([]).
label_bools_one_true([1|_]).
label_bools_one_true([0|L]):-
    label_bools_one_true(L).

naf(Goal):-
    commas_to_list(Goal,GoalList),
    split_body(GoalList,BodyTemp),
    rewrite_body_terms(BodyTemp,Body),
    psic(Body,[],ics(Body,[],_,1)).


unfold_psic @
    psic([[],[],[],[],[],[],[],Atoms],Head,P)
    <=>
    Atoms = [_|_] |
    %status(S,F),
    %draw_graph(S, unfolding_psic, Atoms),
    unfold([psic(Atoms,Head,P)]).

unfold([]):-
    !.
unfold([psic([],Head,P)|MorePSICS]):- !, 
    % MarcoG Bug fix: the cut was after the call to psic.
    % psic is a CHR constraint, that is rewritten to a Prolog predicate
    % that embeds in its definitions the CHR rules that involve the constraint psic.
    % So, by adding a cut after this, we are cutting the choice points inside
    % the rules that involve psic, so sometimes it cuts alternatives in the
    % head!!  
    psic([[],[],[],[],[],[],[],[]],Head,P),
    %impose_head1(Head),
    unfold(MorePSICS).
unfold([psic([naf(Goal)|Body],Head,P)|PSICS]):- !,
    commas_to_list(Goal,GoalList),
    append(Head,[GoalList],NewHead),
    psic([[],[],[],[],[],[],[],Body],NewHead,P),
    unfold(PSICS).
unfold([psic([clp_constraint(Constraint)|MoreBodyAtoms],Head,P)|MorePSICS]):-
    call(Constraint),
    unfold([psic(MoreBodyAtoms,Head,P)|MorePSICS]).
unfold([psic([clp_constraint(Constraint)|_MoreBodyAtoms],_Head,_P)|MorePSICS]):- !,
% MarcoG: metto una pezza, perche' il vincolo a volte e` una restriction
% e non riesce a fare st(C) #<=> 0, pero` si puo` fare st(C) #<=> 0
    invert_constraint(Constraint),
    unfold(MorePSICS).
% MarcoG 12 jul 2006: Sometimes the clause generated with
% unfolding contains the explicit quantification of a
% variable. The quantification is different in PSICs.
% I ignore it for the moment, but maybe it should be
% considered better ...
unfold([psic([quant(_,_)|Body],Head,P)|MorePSICS]):- !,
    unfold([psic(Body,Head,P)|MorePSICS]).

% Federico 08 Jun 2007: Managing abducibles introduced by previous unfolding
unfold([psic([AnAtom|Body],Head,P)|PSICS]):-
	is_SCIFF_RESERVED(AnAtom),
	!,
    P=ics(BodyIn,HeadIn,ID,Prob),
	%ic([AnAtom|Body],Head,Prob,ID),
    convert_to_psic(ic([AnAtom|Body],Head,Prob,ID),BodyIn,HeadIn,NewPSIC),
    call(NewPSIC),
	unfold(PSICS).



unfold([PSIC|MorePSICS]):-
    PSIC=psic([BodyAtom|_],_,_),
    predicate_property(BodyAtom, dynamic),
    !,
    get_candidate_clauses(BodyAtom,Clauses),
    check_forall(Clauses),
    get_unfolded_psics(Clauses,PSIC,UnfoldedPSICS),
    append(UnfoldedPSICS,MorePSICS,NewPSICS),
    unfold(NewPSICS).

unfold([PSIC|MorePSICS]):-
    PSIC=psic([BodyAtom|_],_,_),
    is_sicstus_predicate(BodyAtom),!,
    findall(clause(BodyAtom,[]),BodyAtom,Clauses),
    get_unfolded_psics(Clauses,PSIC,UnfoldedPSICS),
    append(UnfoldedPSICS,MorePSICS,NewPSICS),
    unfold(NewPSICS).

% totally unknown predicate: undefined, not built-in.
unfold([PSIC|MorePSICS]):-
    PSIC=psic([BodyAtom|_],_,_),
    \+predicate_property(BodyAtom, _),!,unfold(MorePSICS).

% MarcoG: The current atom in the PSIC is not a
% defined predicate, nor a built-in. It must have come
% out from unfolding, and must be an H, or an abducible.
% Let us just re-state the IC, so it will be re-parsed
% and quantified by impose_ics. Hope the quantification
% is correct (to do: check if the quantification is correct)


unfold([psic(_,_,_)|_]):-
    writeln('An error occured while applying unfolding transition.'),
    writeln('Please communicate this error to SCIFF developers as soon as possible!'),
    writeln('Many Thanks!'),
    fail.


unfold_nonrecursive([],[]):- !. % The PSIC could not have any fact matching it
% ie., the PSIC can be removed. We represent this fact with the emptylist
% it is used by the recursive call (get_unfolded_nonrecursive can return
% the emptylist)
unfold_nonrecursive(psic(Body,Head),UnfoldedPSIC):-
%    write("Not yet implemented probabilistically"),
    select(Atom,Body,BodyRest),
    predicate_property(Atom,dynamic),
    is_unquantified(Atom), % This way, I am ure it is not invoked by unfolding
    %copy_term(Atom,Atom1), does not strip constraints
    functor(Atom,F,Arity),
    functor(Atom1,F,Arity),  %this strips constraints, in case the IC was called not in the beginning, e.g., by unfold.
    findall((Atom1,BodyClause),clause(Atom1,BodyClause),Clauses),
    nonrecursive(Clauses),!,
    get_unfolded_nonrecursive(Atom,BodyRest,Head,Clauses,[UnfoldedPSIC1]),
    unfold_nonrecursive(UnfoldedPSIC1,UnfoldedPSIC).
unfold_nonrecursive(X,X).

% Currently unfolds eagerly only predicates consisting of exactly one fact.
nonrecursive([(_,true)]).
get_unfolded_nonrecursive(Atom,BodyRest,Head,[(Atom1,_BodyClause)],[UnfoldedPSIC1]):-
    (Atom = Atom1
     ->    UnfoldedPSIC1 = psic(BodyRest,Head)
     ;     UnfoldedPSIC1 = [] % The atom in the body of PSIC does not unify with
                              % the only fact defining it -> the PSIC is removed
    ).

% We assume that all predicates imported from some module are built-in
is_sicstus_predicate(Pred) :- 
    predicate_property(Pred,Prop),
    % Added also interpreted and compiled, so we can invoke predicates defined in
    % SCIFF itself
    memberchk(Prop,[built_in,imported_from(_),interpreted,compiled]).

%is_sicstus_predicate(Pred) :- member(Pred, [ground, var, nonvar, write, nl, =, memberchk, last_state]).
%is_sicstus_predicate(Pred) :- member(Pred, [ground, var, nonvar, write, nl, =, memberchk]).
% End modification

% ORIGINAL VERSION:
/*
unfold([PSIC|MorePSICS]):-
    PSIC=psic([BodyAtom|_],_),
    get_candidate_clauses(BodyAtom,Clauses),
  
    write('Unfolding: BodyAtom:'), nl,
    write(BodyAtom), nl,
    write('Unfolding: Clauses:'), nl,
    write(Clauses), nl,
  
    check_forall(Clauses),
    get_unfolded_psics(Clauses,PSIC,UnfoldedPSICS),
    append(UnfoldedPSICS,MorePSICS,NewPSICS),
    unfold(NewPSICS).
*/
% End original version
% End Modification
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

invert_constraint(st(reif_unify(X,Y,B))):- !,
    Bneg in 0..1,
    Bneg #= 1-B,
    reif_unify(X,Y,Bneg).
invert_constraint(reif_unify(X,Y,B)):- !,
    Bneg in 0..1,
    Bneg #= 1-B,
    reif_unify(X,Y,Bneg).
invert_constraint(Constraint):-
    (Constraint = st(Restriction)
        ->  once(opposite(Restriction,Opp)),
            st(Opp)
        ;   once(opposite(Constraint,Opp)),
            call(Opp)
    ).

check_forall(Term):-
    term_variables(Term,Variables),
    quantify_unquantified(Variables,forall).

quantify_unquantified([],_).
quantify_unquantified([Var|MoreVars],Quant):-
    get_quant(Var,_),
    !,
    quantify_unquantified(MoreVars,Quant).
quantify_unquantified([Var|MoreVars],Quant):-
    set_quant(Var,Quant),
    quantify_unquantified(MoreVars,Quant).


get_unfolded_psics([],_,[]).
get_unfolded_psics([clause(CHead,CBody)|MoreClauses],
           PSIC,
           UnfoldedPSICS):-
    (MoreClauses = []
        ->  PSIC=psic([Atom1|MoreAtoms1],Head1,P)
        ;   ccopy(PSIC,psic([Atom1|MoreAtoms1],Head1,P)) % MG 16 feb 2008: 
                                                % Mi sembra che la ccopy sia necessaria solo se
                                                % ci sono altre clausole
    ),
    (is_term_quantified(Atom1,forall)
     -> (Atom1=CHead
            ->  append(CBody,MoreAtoms1,NewAtoms),
                UnfoldedPSICS=[psic(NewAtoms,Head1,P)|MoreUnfoldedPSICS]
            ;   UnfoldedPSICS=MoreUnfoldedPSICS
        )
    ;   reif_unify(CHead,Atom1,B),
        (B==1 ->    append(CBody,MoreAtoms1,NewAtoms),
                    UnfoldedPSICS=[psic(NewAtoms,Head1,P)|MoreUnfoldedPSICS] ;
         B==0 ->    UnfoldedPSICS=MoreUnfoldedPSICS ;
        (
            (B=1,
            append(CBody,MoreAtoms1,NewAtoms),
            UnfoldedPSICS=[psic(NewAtoms,Head1,P)|MoreUnfoldedPSICS])
        ;
            (B=0,
            UnfoldedPSICS=MoreUnfoldedPSICS)
        ))
    ),
    get_unfolded_psics(MoreClauses,PSIC,MoreUnfoldedPSICS).
       


get_candidate_clauses(Atom,Clauses):-
    functor(Atom,F,N),
    findall(clause(Head,Body),
        (
          functor(Head,F,N),
          clause(Head,Body1),
          fn_ok(Atom,Head),
          (Body1=true->
              Body=[];
              commas_to_list(Body1,Body))
        ),
        Clauses).



impose_head1(Head):-
    quantify_unquantified(Head),
    term_variables(Head,Variables),
    flag_variables(Variables),
    (Head=[Disj]-> impose_head_disjunct(Disj) ;
     Head=[] -> fail ;
        nondeterministic(impose_head(Head))).


quantify_unquantified(Head):-
    ics_scan_head(Head,[],HV1),
    adjust_variable_list(HV1,HV),
    % No more repeated variables now (eliminated when SICS were loaded)
    quantify_unquantified_variables(HV).

quantify_unquantified_variables([]).
quantify_unquantified_variables([variable(Variable,_)|MoreVariables]):-
    get_quant(Variable,_),
    !,
    quantify_unquantified_variables(MoreVariables).
quantify_unquantified_variables([Variable|MoreVariables]):-
    decide_variable_quantification(Variable,Quantification),
    attach_variable_quantification(Variable,Quantification),
    quantify_unquantified_variables(MoreVariables).

  
%impose_head([]):- fail.
impose_head([H|_]):-
    impose_head_disjunct(H).
impose_head([_|T]):-
    impose_head(T).

impose_head_disjunct([]).
impose_head_disjunct([clp_constraint(C)|Tail]):- !,
    st(C), impose_head_disjunct(Tail).
impose_head_disjunct([Head|Tail]):-
    %Head=..[Functor|Arguments],
    %length(Arguments,NofArgs),
    %functor(Head,Functor,NofArgs),
    isabducible(Head),
    !,
    abduce(Head),
    impose_head_disjunct(Tail).
impose_head_disjunct([Head|Tail]):-
    call(Head),
    impose_head_disjunct(Tail).

% duplicated both with 2 and 3 arguments, because they might occur in both ways
isabducible(h(_)).
isabducible(h(_,_)).  
isabducible(e(_,_)).
isabducible(e(_)).
isabducible(en(_)).
isabducible(en(_,_)).
isabducible(note(_)).
isabducible(note(_,_)).
isabducible(noten(_)).
isabducible(noten(_,_)).
isabducible(abd(_)).
isabducible(abd(_,_)).

is_SCIFF_RESERVED(noth(_)) :- !.
is_SCIFF_RESERVED(noth(_,_)) :- !.
is_SCIFF_RESERVED(X) :-
	isabducible(X).


violation @
    h(F,HEvent),
    pending(en(F,EEvent)) # _pending
    ==>
    fn_ok(HEvent,EEvent)
    |
    %ccopy(p(EEvent,ETime),p(EEvent1,ETime1)),  MA QUESTA CCOPY E` NECESSARIA? SI`, SE violation_causes_failure=off. SPOSTATO NEL CASE_ANALYSIS_VIOLATION
    case_analysis_violation(F,HEvent,EEvent,_pending).
/* Old version
case_analysis_violation(HEvent,HTime,EEvent,ETime,
              EEvent1,ETime1,_pending):-
    reif_unify(p(HEvent,HTime),p(EEvent1,ETime1),1),
    remove_constraint(_pending),
    viol(en(EEvent,ETime)).
case_analysis_violation(HEvent,HTime,_,_,EEvent1,ETime1,_):-
    reif_unify(p(HEvent,HTime),p(EEvent1,ETime1),0).
*/
% New version, more efficient
case_analysis_violation(F,HEvent,EEvent,_):- 
    get_option(violation_causes_failure, OptFail),
    (OptFail = yes
        ->  reif_unify(p(HEvent),p(EEvent),0)
        ;   ccopy(p(EEvent),p(EEvent1)),
			reif_unify(p(HEvent),p(EEvent1),B),
            (   B=0
            ;   B=1,
                %remove_constraint(_pending),
                viol(en(F,EEvent1))
            )
    ).





load_ics:-
    prob_granularity(Granularity),
    findall(ic(Head,Body,Pinteger),
        (
            ics(Head,Body,Pfloat),
%MG: if the probability is a float, then we convert it into integer, intending it as the numerator of a fraction having prob_granularity/1 as denominator.
% Otherwise, we assume it is already intended as an integer (also in case it is a variable)
            (float(Pfloat)
               -> Pinteger is round(Pfloat*Granularity)
                ; Pinteger = Pfloat
            )
        ),
        ICSs),
    decorate_with_increasing_number(ICSs,ICSd,1),
    call_list(ICSd).

decorate_with_increasing_number([],[],_).
decorate_with_increasing_number([A|ICSs],[Ad|ICSd],N):-
    A =.. L,
    append(L,[N],L1),
    Ad =.. L1,
    N1 is N+1,
    decorate_with_increasing_number(ICSs,ICSd,N1).

history:-
    history_is_empty(yes),
    !.
history:-
    findall(h(Event),
        hap(Event),
        History),
    set_term_quantification(History, existsf),
    call_list(History).


call_list([]).
call_list([IC|MoreICs]):-
    call(IC),
    call_list(MoreICs).


/* Abduction */



abduce(Abducible):-
    term_variables(Abducible,Variables),
    flag_variables(Variables),
    call(Abducible).

 
flag_variables([]).  
flag_variables([Var|MoreVars]):-
    get_quant(Var,Q),
    ((Q=forallf ; Q=existsf) -> true
    ;   flag_quant(Q,NewQ),
        set_quant(Var,NewQ)
    ),
    flag_variables(MoreVars).

flag_quant(exists,existsf).
flag_quant(forall,forallf).

/* old version, less efficient
flag_variables([]). 
flag_variables([Var|MoreVars]):-
    get_quant(Var,existsf),
    !,
    flag_variables(MoreVars).
flag_variables([Var|MoreVars]):-
    get_quant(Var,forallf),
    !,
    flag_variables(MoreVars).
flag_variables([Var|MoreVars]):-
    get_quant(Var,exists),
    !,
    quant(Var,existsf),
    flag_variables(MoreVars).
flag_variables([Var|MoreVars]):-
    get_quant(Var,forall),
    quant(Var,forallf),
    flag_variables(MoreVars).
*/



%MarcoG 9 may 2005
% The 1st clause used to be:
%check_unification(T1,T2,B,_,_):-
%    ccopy(p(T1,T2),p(U1,U2)),
%    reif_unify(U1,U2,B),
%    reif_unify(T1,T2,B),!.
% but in this case
% ?- existsf(U1), U1 #< P, e(un(a),U1), forallf(X), forallf(U2), st(U2,U2#<U1), st(U2,U2#>0), en(un(X),U2).
% results in X=a!!
% I only unify the copies.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Modified by Federico, 30-10-2006
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Old version:
/*


check_unification(T1,T2,B,_,_):-
    ccopy(p(T1,T2),p(U1,U2)),
    reif_unify(U1,U2,B),!.
 check_unification(T1,T2,_,S1,S2):-
    T1=..[_|A1],
    T2=..[_|A2],
    R1=..[S1|A1],
    R2=..[S2|A2],
    inconsistent(R1,R2),
    (	current_predicate(printConstraints/0)
     	->	printConstraints
     	;		true
    ),
    fail.
*/


% New Version:
% DANGER!!! This predicate is defined also in sciff_java_gui.pl
% PLEASE DO NOT MODIFY IT UNLESS YOU REALLY KNOW WHAT ARE YOU DOING!
% KEEP COHERENT THE TWO VERSIONS OF THIS PREDICATE !!!
:- chr_constraint
   inconsistent/2.

check_unification(T1,T2,B,e,note):- !,
    reif_unify(T1,T2,B).
check_unification(T1,T2,B,e,en):- !,
    ccopy(T2,U2),
    reif_unify(T1,U2,B).
check_unification(T1,T2,B,_,_):-
    ccopy(p(T1,T2),p(U1,U2)),
    reif_unify(U1,U2,B).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



not_consistency_e @
    e(_,EEvent),
    note(NotEEvent)
    ==>
    check_unification(p(EEvent),p(NotEEvent),0,e,note).


not_consistency_en @
    en(_,EnEvent),
    noten(NotEnEvent)
    ==>
    check_unification(p(EnEvent),p(NotEnEvent),0,en,noten).

:- chr_constraint
	close_history/0.



/*  version SICStus 3.9
propagation_noth @
    close_history,
    psic([[],[NotH|MoreNotH],[],[],[],[],[],A],Head) # _psic
    ==>
    true
    &
    Body=[[],[NotH|MoreNotH],[],[],[],[],[],A],
    ccopy(p(Body,Head),p(Body1,Head1)),
    propagate_noth(Body1,Body2)
    |
    psic(Body2,Head1).
%    pragma
%    passive(_psic).
*/

% Version for SICStus 4. No more tell guards: Check if it is correct!!!
propagation_noth @
    close_history,
    psic([[],[NotH|MoreNotH],[],[],[],[],[],A],Head,P)
    ==>
    Body=[[],[NotH|MoreNotH],[],[],[],[],[],A],
    ccopy(p(Body,Head),p(Body1,Head1)),
    propagate_noth(Body1,Body2)
    |
    (P<1 -> writeln("Probabilistic notH not implemented") ; true),
    psic(Body2,Head1).



propagate_noth([H,NotH,E,NotE,EN,NotEN,Abd,A],
           [H,NewNotH,E,NotE,EN,NotEN,Abd,A]):-
    noth_propagation(NotH,NewNotH).

noth_propagation([NotH|MoreNotHs],NewNotH):-
    noth_success(NotH),
    !,
    noth_propagation1(MoreNotHs,NewNotH).
noth_propagation([NotH|MoreNotHs],[NotH|MoreNewNotHs]):-
    noth_propagation(MoreNotHs,MoreNewNotHs).

noth_propagation1([],[]).
noth_propagation1([NotH|MoreNotHs],NewNotH):-
    noth_success(NotH),
    !,
    noth_propagation1(MoreNotHs,NewNotH).
noth_propagation1([NotH|MoreNotHs],[NotH|MoreNewNotHs]):-
    noth_propagation1(MoreNotHs,MoreNewNotHs).

noth_success(noth(Event)):-
    universal_variables_in_term(noth(Event),UnivVariables),
    (UnivVariables=[] ->
        \+(has_happened(Event));
        universal_variables_happened(Event,TermList), % In realta` il D8 dice che 
        % dovrei rendere universali tutte le variabili, escluse le esistenziali non flagged. 
        PUniv=..[p|UnivVariables],
        % Qui devo rendere flagged tutte le variabili che sono universali (in teoria tutte escluse le exist non flagged)
        flag_variables(UnivVariables),
        impose_noth_not_unif(TermList,PUniv)).

impose_noth_not_unif([],_).
impose_noth_not_unif([Term|MoreTerms],PUniv):-
    set_restriction(reified_unif:reif_unify(Term,PUniv,0)),
    %reif_unify(Term,PUniv,0),
    impose_noth_not_unif(MoreTerms,PUniv).

universal_variables_in_term(Term,UnivVariables):-
    term_variables(Term,Vars),
    keep_universals(Vars,UnivVariables).

keep_universals([],[]).
keep_universals([Var|MoreVars],[Var|MoreUnivVars]):-
    is_universal(Var),
    !,
    keep_universals(MoreVars,MoreUnivVars).
keep_universals([_|MoreVars],UnivVars):-
    keep_universals(MoreVars,UnivVars).

universal_variables_happened(Event,TermList):-
    findall(Term,
        (
          ccopy(p(Event),p(Event1)),
          universal_variables_in_term(p(Event1),UnivVars),
          has_happened(Event1),
          Term=..[p|UnivVars]
          ),
        TermList).

has_happened(Event):-
    findall_constraints(h(_,Event),L),
	member(h(_,Event),L).

%% COMPLIANT HISTORY GENERATION: 20 JUL 04

/* versione MarcoA
fulfiller1 @
    close_history,
    pending(e(Event,Time))
    ==>
%    write('Inserted '),write(h(Event,Time)),nl,
  %  write_history,
    h(Event,Time).
*/

pending_e @
    e(F,Event)
    ==>
    pending(e(F,Event)).

%----------------------------------------------------------
% FULFILLMENT RULES
%----------------------------------------------------------
%new fdet version
fulfillment @
    h(F,HEvent),
    pending(e(F,EEvent)) # _pending
    ==>
    fn_ok(HEvent,EEvent)
    |
    %ccopy(p(EEvent,ETime),p(EEvent1,ETime1)), Non necessary: all variables existentially quant.
    case_analysis_fulfillment(F,HEvent,EEvent,_pending).

case_analysis_fulfillment(F,HEvent,EEvent,_):-
    % HEvent=EEvent, HTime=ETime,
    % If we use unification, it triggers all the CHR constraints involving the variables
    % before terminating the execution. With g-SCIFF, it will trigger fulfiller before
    % removing the pending expectation. Better use reif_unify.
    % 4 Aug 07: Actually even with reif_unify fulfiller is triggered too early.
    % Let us anticipate remove_constraint
    %remove_constraint(_pending),
    %term_unify(HEvent,EEvent),
    %eq(HTime,ETime),
    reif_unify(p(HEvent),p(EEvent),1),
    (
    	fdet(e(Term)),
    	subsumeschk(e(Term),e(EEvent))
    ->
    	!
    ;
    	true
    ),
    fulf(e(F,EEvent)).
case_analysis_fulfillment(_,HEvent,EEvent1,_):-
    reif_unify(p(HEvent),p(EEvent1),0).

% submission to special issue Fundamenta Informaticae following CILC2017
fulfillment_nested @
    h(F,HEvent),
    pending(e(F,EEvent)) # _pending
    ==>
    sciff_option(nested_e,on)
%    fn_ok(HEvent,EEvent)
    |
    %ccopy(p(EEvent,ETime),p(EEvent1,ETime1)), Non necessary: all variables existentially quant.
    case_analysis_fulfillment_nested(F,HEvent,EEvent,_pending).

case_analysis_fulfillment_nested(F,HEvent,EEvent,_):-
    nested_reif_unify(HEvent,EEvent,1),
%    reif_unify(p(HEvent,HTime),p(EEvent,ETime),1),
    (
    	fdet(e(Term)),
    	subsumeschk(e(Term),e(EEvent))
    ->
    	!
    ;
    	true
    ),
    fulf(e(F,EEvent)).
case_analysis_fulfillment_nested(_,HEvent,EEvent,_):-
    nested_reif_unify(HEvent,EEvent,0).
%    reif_unify(p(HEvent,HTime),p(EEvent1,ETime1),0).

nested_reif_unify(HEvent,EEvent,Bool):-
    nonvar(EEvent), nonvar(HEvent),!,
    (EEvent = discharged(e(_,EEvent1))
    ->  nested_reif_unify(HEvent,EEvent1,Bool)
    ;   reif_unify(p(HEvent),p(EEvent),Bool)
    ).
nested_reif_unify(_HEvent,_EEvent,_Bool):-
    writeln("Unimplemented feature: nested_reif_unify with variable expectation").


fulfillment_allows @
    h(F,do(HX,HRec,HAction,HD)),
    pending(e(F,do(Expecter,EX,ERec,EAction,ED))) # _pending
    ==>
    fn_ok(p(do(HX,HRec,HAction,HD)),p(do(EX,ERec,EAction,ED)))
    |
    %ccopy(p(EEvent,ETime),p(EEvent1,ETime1)), Non necessary: all variables existentially quant.
    case_analysis_fulfillment(F,do(Expecter,HX,HRec,HAction,HD),do(Expecter,EX,ERec,EAction,ED),_pending).

/*
fulfillment @
    h(HEvent,HTime),
    pending(e(EEvent,ETime)) # _pending
    ==>
    fn_ok(HEvent,EEvent)
    |
    %ccopy(p(EEvent,ETime),p(EEvent1,ETime1)), Non necessary: all variables existentially quant.
    case_analysis_fulfillment(HEvent,HTime,EEvent,ETime,_pending).

case_analysis_fulfillment(HEvent,HTime,HEvent,HTime,_pending):-
    remove_constraint(_pending),
    %reif_unify(p(HEvent,HTime),p(EEvent,ETime),1),
    (sciff_option(fdet,on)->!;true),
    fulf(e(EEvent,ETime)).
case_analysis_fulfillment(HEvent,HTime,EEvent1,ETime1,_):-
    reif_unify(p(HEvent,HTime),p(EEvent1,ETime1),0).
*/


/* versione MarcoG */
fulfiller1 @
    (close_history)
    \
    (pending(e(F,Event)))
    <=>
%        write('Inserted '),write(h(Event,Time)),
    sciff_option(fulfiller,on)
    %findall_constraints(nondeterministic(_),[]) bug fix: fulfilment does not use nondeterinistic
    |
    fulf(e(F,Event)),
    h(F,Event).

%Constraints used by AlLoWS
:- chr_constraint
	gen_phase/0, end_gen_phase/0, remove_exp/0.

fulfiller_rationality @
    (gen_phase) \
    pending(e(F,do(X,X,Rec,Action,D)))
    <=>
    %sciff_option(fulfiller_rationality,on) |
    fulf(e(F,do(X,X,Rec,Action,D))),
    h(F,do(X,Rec,Action,D)).

fulfiller_chor @
    (gen_phase),
    pending(e(F,do(chor,X,Rec,Action,D))) % # _pending
    ==>
    %sciff_option(fulfiller_rationality,on) |
    testing(WS), set_term_quantification(WS,existsf),
    reif_unify(WS,X,Bool),
    (Bool=0;Bool=1),
    (Bool=0
      ->    %remove_constraint(_pending),
            fulf(e(F,do(chor,X,Rec,Action,D))),
            h(F,do(X,Rec,Action,D))
      ;     true).

end_gen_phase @
    end_gen_phase \
    gen_phase <=> true.

write_history:-
    %findall_constraints(h(send(_,_,_,_),_),L), MarcoG: why only send?
    findall_constraints(h(_,_),L), %%%%%% TODO
    print_chr_list(L,'\n').

write_normal_abducibles:-
		findall_constraints(abd(_,_),L),
    print_chr_list(L,'\n').

write_positive_expectations:-
		findall_constraints(e(_,_),L),
    print_chr_list(L,'\n').

write_violations:-
		findall_constraints(viol(_,_),L),
    print_chr_list(L,'\n').

write_events_not_expected:-
		findall_constraints(not_expected(_,_),L),
    print_chr_list(L,'\n').

/*
closure_e @
    (close_history)
    \
    (pending(e(Event,Time)) # _pending)
    <=>
    viol(e(Event,Time))
    pragma
    passive(_pending).
*/


closure_e @
    (close_history),
    (pending(e(F,Event)) ) %# _pending)
    ==>
    true | % this guard has been added to avoid a mis-interpretation of the chr compiler
    (get_option(violation_causes_failure, yes)
    		->	fail
    		; 	(%remove_constraint(_pending),
    				 viol(e(F,Event))
    				)
    ).
    %pragma passive(_pending). Does not work with violation_causes_failure -> no

/*
closure_e @
    (close_history)
    \
    (pending(e(Event,Time)) # _pending)
    <=>
    get_option(violation_causes_failure, no)
    |
    viol(e(Event,Time))
    pragma passive(_pending).
*/

:- chr_constraint
	not_expected/1.


not_expected_gen @
		(h(F,HEvent))
		==>
		sciff_option(allow_events_not_expected, no)
		|
		not_expected(h(F,HEvent)).



not_expected_remove @
		fulf(e(F,Event))
		\
		not_expected(h(F,Event))
		<=>
		true.

% If an event is expected both by the WS and the Chor,
% then it is no longer not_expected
not_expected_remove_allows1 @
		(fulf(e(F,do(chor,EX,ERec,EAction,ED))),
		fulf(e(F,do(WS,EX,ERec,EAction,ED))))
		\
		not_expected(h(F,do(EX,ERec,EAction,ED)))
		<=> testing(WS) |
		true.

% If an event is expected only by the choreography,
% and it does not involve the WS under test, then
% the event is expected
not_expected_remove_allows2 @
		fulf(e(F,do(chor,EX,ERec,EAction,ED)))
		\
		not_expected(h(F,do(EX,ERec,EAction,ED)))
		<=> %write('******** TRYING ***************'),
            testing(WS), nonvar(EX), EX \= WS, nonvar(ERec), ERec \= WS |
		true.


% violation is imposed only if the corresponding flag is set to true
protocol_e @
		(close_history) \
    (not_expected(h(F,Event)) # _not_expected)
    <=>
    true %added to avoid a possible chr bug
    |
    (	get_option(violation_causes_failure, yes)
    	->	fail
    	;		%remove_constraint(_not_expected),
    			viol(not_expected(h(F,Event)))
    )
    pragma passive(_not_expected).
    

% End Modification
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


closure_en @
    (close_history)
    \
    (pending(en(F,Event)) # _pending)
    <=>
    fulf(en(F,Event))
    pragma
    passive(_pending).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MODIFIED BY FEDERICO
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Removed because of an optimization introduced by MarcoG
% If violation causes failure then failure is imposed immediately
% without adding the chr_constraint viol(_).
% Otherwise the chr_constraint viol(_) is imposed, and no failment is imposed.
%
% Violations are generated by the following chr_rules:
% - case_analysis_violation (EN with a corresponding H)
% - closure_e (close_history, and E without H)
% - ct_time_constraint (E and due to the current_time, no H is possible anymore)
% - protocol_e (H without E)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
/*
failure_for_violation @
    viol(_)
    ==>
    (	current_predicate(printConstraints/0)
     	->	printConstraints
     	;		true
    ),
   get_option(violation_causes_failure, no).
   */
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%






%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CURRENT TIME MANAGEMENT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% :- chr_constraint
%     current_time/1.

% current_time_update @
%     (h(_,current_time))
%     \
%     (current_time(_OldCT) # _current_time)
%     <=>
%     current_time(CT)
%     pragma
%     passive(_current_time).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Modified by Federico, 20061102
%
% Original version:
/*
ct_time_constraint @
    current_time(CT),
    pending(e(Event,Time)) # _pending
    ==>
    impose_time_constraint(Time,CT)
    pragma
    passive(_pending).

constraints
    gt_current_time/2.

impose_time_constraint(Time,CT):-
    Time #>= CT,
    !.
impose_time_constraint(Time,CT):-
    gt_current_time(Time,CT),
    (	current_predicate(printConstraints/0)
     	->	printConstraints
     	;		true
    ),
    fail.
*/

% ct_time_constraint @
%     current_time(CT),
%     pending(e(F,Event,Time)) # _pending
%     ==>
%     impose_time_constraint(Time,CT, e(F,Event,Time), _pending)
%     pragma
%     passive(_pending).

% impose_time_constraint(Time,CT, e(F,Event,Time), _):-
%     ( geq(Time,CT)
%     	->	true
%     	;		( get_option(violation_causes_failure, yes)
%     				->	fail
%     				;	%remove_constraint(_pending),	
%                         viol(gt_current_time(e(F,Event,Time), failed_to_impose_greater_than(Time, CT)))
%     			)
%     ).


%end @ phase(deterministic) <=> findall_constraints(nondeterministic(_),[]) | true.
switch2nondet @ phase(deterministic) <=> phase(nondeterministic).
/*switch2det @ phase(nondeterministic) \ nondeterministic(G) <=>
    call(G).
    %phase(deterministic).*/

switch2det @ phase(nondeterministic) , nondeterministic(G) <=>
    call(G),
    phase(deterministic).


remove_phase \ phase(_) <=> true.
remove_phase <=> true.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



build(SOKB,ICS,History):-
   translate_sokb(SOKB,'./sokb.pl'),
    translate_ics(ICS,'./ics.pl'),
    translate_history(History,'./history.pl').

build(ExampleName):-
    atom_concat(ExampleName,'/',A1),
    atom_concat(A1,ExampleName,A2),
    atom_concat(A2,'_sokb.pl',SOKB),
    atom_concat(A2,'_ics.txt',ICS),
    atom_concat(A2,'_history.txt',History),
    build(SOKB,ICS,History).
build(ExampleName,SubExample):-
    atom_concat(ExampleName,'/',A1),
    atom_concat(A1,ExampleName,A2),
    atom_concat(A2,'_sokb.pl',SOKB),
    atom_concat(A2,'_ics.txt',ICS),
    atom_concat(A2,'_history_',A3),
    atom_concat(A3,SubExample,A4),
    atom_concat(A4,'.txt',History),
    build(SOKB,ICS,History).

build1(ExampleName):-
    atom_concat('../examples/',ExampleName,A0),
    atom_concat(A0,'/',A1),
    atom_concat('../protocols/',ExampleName,B0),
    atom_concat(B0,'/',B1),
    atom_concat(A1,ExampleName,A2),
    atom_concat(B1,ExampleName,B2),
    atom_concat(B2,'_sokb.pl',SOKB),
    atom_concat(B2,'_ics.txt',ICS),
    atom_concat(A2,'_history.txt',History),
    build(SOKB,ICS,History).
build1(ExampleName,SubExample):-
    atom_concat('../examples/',ExampleName,A0),
    atom_concat(A0,'/',A1),
    atom_concat('../protocols/',ExampleName,B0),
    atom_concat(B0,'/',B1),
    atom_concat(B1,ExampleName,B2),
    atom_concat(A1,ExampleName,A2),
    atom_concat(B2,'_sokb.pl',SOKB),
    atom_concat(B2,'_ics.txt',ICS),
    atom_concat(A2,'_history_',A3),
  
    atom_concat(A3,SubExample,A4),
    atom_concat(A4,'.txt',History),
    build(SOKB,ICS,History).

build2(ExampleName):-
    atom_concat('../examples/',ExampleName,A0),
    atom_concat(A0,'/',A1),
    atom_concat('../protocols/',ExampleName,B0),
    atom_concat(B0,'/',B1),
    atom_concat(A1,ExampleName,_A2),
    atom_concat(B1,ExampleName,B2),
    atom_concat(B2,'_sokb.pl',SOKB),
    atom_concat(B2,'_ics.txt',ICS),
    build3(SOKB,ICS).
  
build3(SOKB,ICS):-
   translate_sokb(SOKB,'./sokb.pl'),
    translate_ics(ICS,'./ics.pl').
/*
 
:- dynamic(numero_soluzioni/1).
 
conta_soluzioni :-
    retractall(numero_soluzioni(X)),
    assert(numero_soluzioni(0)),
    run,
    numero_soluzioni(N),
    N1 is N + 1,
    write('trovata una soluzione ('),write(N1),write(')'),nl,
    retract(numero_soluzioni(N)),
    assert(numero_soluzioni(N1)),
    fail.
  */

prob(Goal,L1,P):- %Lworlds al posto di L per noisy_or
    findall(world(WorldID,Lunif,P),(Goal,query_world(WorldID,P),findall_unif_constraints(Lunif)),L),
%MG: La lista L contiene dei termini world/3, che hanno come secondo argomento la lista Lunif, che contiene i vincoli di unificazione e disunificazione
% Per ora la remove_subsumed fornisce una lista che contiene termini world/2, senza la lista Lunif.
% Per implementare la compute_prob, si puo` usare direttamente la lista L
%    remove_subsumed(L,Lworlds),
%%BUG: non devo fare sempre il noisy-or, solo se sono indipendenti
% Due mondi possono essere indipendenti (e.g. W1 = added IC1, W2 = added IC2)
% possono essere incompatibili (eg. W1 = added IC1, W2 = removed IC1)
% possono avere una parte in comune (eg. W1 = [added IC1, added IC2], W2 = [added IC1, removed IC3])
% Come faccio ad accorgermi che due mondi sono incompatibili?
% Il mondo [added p(X) -> q(X)] e il mondo [removed p(Y) -> q(Y)] sono incompatibili?
% Considera che esiste anche il mondo [added p(X) -> q(X), removed p(Y) -> q(Y)] !
%    noisy_or(Lworlds,P).
    check_worlds(L,L1),
    compute_prob(L1,P).

prob(Goal,Delta,Lworlds1,P):-
    findall(world(WorldID,Lunif,P,Delta),
        (Goal,
         query_world(WorldID,P),
         findall_unif_constraints(Lunif),
         get_abducibles_sorted(abd(_,_),LAbd),
         get_abducibles_sorted(e(_,_),LE),
         get_abducibles_sorted(en(_,_),LEn),
         get_abducibles_sorted(note(_,_),LNotE),
         get_abducibles_sorted(noten(_,_),LNotEn),
         Delta=[LE,LNotE,LEn,LNotEn,LAbd]
        )
    ,L),
    group_deltas(L,Groups),
    member([Delta,Lworlds],Groups),
    maplist(convert_probabilities_to_float_world,Lworlds,LworldsFloatProb),
    check_worlds(LworldsFloatProb,Lworlds1),
    compute_prob(Lworlds1,P).

convert_probabilities_to_float_world(world(WorldID,Lunif,Pint),world(WorldIDfloat,Lunif,Pfloat)):-
    (integer(Pint)
     -> prob_int_to_float(Pint,Pfloat),
        maplist(convert_probabilities_to_float_ics,WorldID,WorldIDfloat)
     ;  Pfloat=Pint
    ).

convert_probabilities_to_float_ics(ics(AddRem,B,H,ICID,ProbInt),ics(AddRem,B,H,ICID,ProbFloat)):-
    (integer(ProbInt)
     -> prob_int_to_float(ProbInt,ProbFloat)
     ;  writeln_debug('I was expecting an integer probability, while found'),
        writeln_debug(ProbInt), write_debug('in '),
        writeln_debug(ics(AddRem,B,H,ICID,ProbInt))
    ).

prob_int_to_float(Pint,Pfloat):-
    prob_granularity(Granularity),
    Pfloat is Pint/Granularity.

get_abducibles_sorted(Pattern,Sorted):-
    findall_constraints(Pattern,Unsorted),
    sort(Unsorted,Sorted).

:- use_module(library(apply)).
:- use_module(library(yall)).

group_deltas([],[]). 
group_deltas([H|T],[[Delta,Lworlds]|LGroups]):-
    H = world(_WorldID,_Lunif,_P,Delta),
    partition(same_delta(Delta),[H|T],SameDelta,DifferentDelta),
/*    maplist(([X,Y]>>(X=world(WorldID,Lunif,P,Delta), % Remove the parameter Delta
                     Y=world(WorldID,Lunif,P)
                    )
            )
        ,SameDelta,Lworlds), */
    maplist(strip_last,SameDelta,Lworlds),
    group_deltas(DifferentDelta,LGroups).

strip_last(world(WorldID,Lunif,P,_Delta),world(WorldID,Lunif,P)).


same_delta(Delta,world(_WorldID,_Lunif,_P,Delta1)):-
% Siccome sono gia` ordinati rispetto all'ordinamento standard @<, qui e` sufficiente controllare che siano una variante uno dell'altro
    Delta =@= Delta1.

remove_subsumed(L,Lout):-
    maplist(strip_unif_constr,L,L1),
    remove_subsumed(L1,[],Lout).

strip_unif_constr(world(WorldID,_Lunif,P),world(WorldID,P)).

%%MG In realta` cosi` non va bene. Potrei avere due mondi
%% W1 = [added p(X)-->q(X), added r(X)-->s(X)]
%% W2 = [added p(X)-->q(X), added r(Y)-->s(Y), added f(Z)-->s(Z)]
%% Questi due mondi non si sussumono l'un l'altro: nel primo ho due IC istanziati con la stessa X
%% nel secondo i primi due IC fanno riferimento a due istanziazioni diverse
%% Quindi verificare se i singoli IC sono sussunti non e` sufficiente.
remove_subsumed([],L,L).
remove_subsumed([World|L],Lin,Lout):-
    add_to_non_subsumed_list(World,Lin,Ltemp),
    remove_subsumed(L,Ltemp,Lout).

add_to_non_subsumed_list(W,[],[W]):-!.
add_to_non_subsumed_list(world(NewWorld,_),[W1|Lin],Lout):-
    W1 = world(CurrentWorld,_),
    is_subsumed(NewWorld,CurrentWorld),!,
    [W1|Lin]=Lout.
add_to_non_subsumed_list(W,[W1|Lin],Lout):-
    W = world(NewWorld,_),
    W1 = world(CurrentWorld,_),
    is_subsumed(CurrentWorld,NewWorld),!,
    add_to_non_subsumed_list(W,Lin,Lout).
add_to_non_subsumed_list(W,[W1|Lin],[W1|Lout]):-
    add_to_non_subsumed_list(W,Lin,Lout).

is_subsumed(_,[]):-!.
is_subsumed(L1,[ics(AddRem,Body,_Head,ID,P)|L2]):-
    member(ics(AddRem,Body1,_Head1,ID,P),L1),
    subsumes_term(Body,Body1),
%    Body1 =@= Body, % Body is a variant of Body1
    !,
    is_subsumed(L1,L2).

noisy_or([],0).
noisy_or([world(_,Pw)|L],Pout):-
    noisy_or(L,Ptemp),
    Pout is Pw+(1-Pw)*Ptemp.

run:-
    init_world,
    load_ics,
    %current_time(0),
    society_goal,
    history,
    phase(deterministic),
    close_history.
%    print_world_id.
run(A,B):-
    init_world,
    load_ics,
    %current_time(0),
    society_goal(A,B),
    history,
    phase(deterministic),
    close_history.
%    print_world_id.

run_no_close:-
    init_world,
    load_ics,
    %current_time(0),
    society_goal,
    history,
    phase(deterministic).
%    print_world_id.
run_no_close(A,B):-
    init_world,
    load_ics,
    %current_time(0),
    society_goal(A,B),
    history,
    phase(deterministic).
%    print_world_id.

try_number(0).
try_number(X):-
    try_number(Y),
    X is Y+1.

iterative_deepening(Goal):-
    try_number(Depth),
    write('***************'), writeln(depth(Depth)),
    max_depth(Depth),
    call(Goal).

iter:-
    init_graph('proof.dot',_Stream),
    statistics(runtime,_),
    load_ics,
    iterative_deepening(
        (society_goal,
        history,
        phase(deterministic),
        once((
            write('grounding time\n'),  
            ground_time,
            write('grounding 1\n'),
            make_choice
            
            ))
        )),
    statistics(runtime,[_,Time]),
    writeln(runtime(Time)).

iter_gsciff:-
    init_graph('proof.dot',_Stream),
    statistics(runtime,_),
    load_ics,
    society_goal,
    N in 0..1000,
    indomain(N), write('************************* new happen ******************************'),
        writeln(N),
    add_history_el(N),
    phase(deterministic),
    close_history,
    write('grounding\n'),
    make_choice,
    statistics(runtime,[_,Time]),
    writeln(runtime(Time)).

iter_gsciff_hist:-
    init_graph('proof.dot',_Stream),
    statistics(runtime,_),
    load_ics,
    society_goal,
    history,
    phase(deterministic),
    N in 0..1000,
    indomain(N), write('************************* new happen ******************************'),
        writeln(N),
    add_history_el_sym(N,_),
    close_history,
    statistics(runtime,[_,Time]),
    writeln(runtime(Time)).

% must be invoked with functor & arity: only atoms H(f(...),T) will be abduced.
% Necessary with new version of SCIFF
iter_gsciff_hist(Functor/Arity):-	
    init_graph('proof.dot',_Stream),
    statistics(runtime,[Time1,_]),
    load_ics,
    society_goal,
    history,
    phase(deterministic),
    N in 0..1000,
    indomain(N), write('************************* new happen ******************************'),
        writeln(N),
	statistics(runtime,[Time2,_]), Time is Time2-Time1, writeln(runtime(Time)),
    add_history_el_sym(N,_,Functor/Arity),
    close_history.
    %statistics(runtime,[_,Time]),
    %writeln(runtime(Time)).

:- chr_constraint
	remove_close_history/0.

remove_close_history, close_history <=> true.

% must be invoked with functor & arity: only atoms H(f(...),T) will be abduced.
% Necessary with new version of SCIFF
bmc_accountable(Functor/Arity,LAgents,Verbosity):-	
	iter_gsciff_hist(Functor/Arity),
	ground_performer(LAgents),
	end_ground_performer,
	(Verbosity>1 -> write_history ; true),
	remove_exp,
	remove_close_history,
	%findall_constraints(viol(_),Lviolati), writeln(violati(Lviolati)),
	%findall_constraints(e(_,_,_),LE), writeln(expected(LE)),
	%findall_constraints(e(_,_,_),LE), writeln(expected(LE)),
	%printAllCHRconstraints,
	findall(Culprits,
		(   load_ics,
			society_goal,
			%phase(deterministic),
			close_history,
			get_culprits(Culprits,LAgents)
		),
		LCulprits),
	(memberchk([],LCulprits)
		->	(Verbosity>0->writeln('No violation');true), fail
		;	multi_intersection(LCulprits,FinalCulprits),
			(FinalCulprits = []
			 -> writeln('***** No culprits! ****')
			 ;  (Verbosity>0->write('culprits: '), writeln(FinalCulprits) ;true), fail
			)
	).
	

printAllCHRconstraints:- find_chr_constraint(Constraint), write(Constraint), write('\t'), fail.
printAllCHRconstraints.

:- chr_constraint ground_performer/1, end_ground_performer/0.

ground_performer(LAgents), h(_,Term) ==> arg(1,Term,X), var(X) | member(X,LAgents).

ground_performer(_), end_ground_performer <=> true.

get_culprits(LCulprits,LAgents):-
	findall_constraints(viol(_),Lviol),
	map(viol_culprit,Lviol,LCulpritsDupl),
	groundFromList(LCulpritsDupl,LAgents),
	rem_dupl(LCulpritsDupl,LCulprits).

% groundFromList(L,Ldomain)
% makes each element in L ground. If it is already ground -> ok, otherwise nondeterministically select an element in Ldomain
groundFromList([],_).
groundFromList([H|T],List):-
	ground(H), !, groundFromList(T,List).
groundFromList([H|T],List):-
	member(H,List),
	groundFromList(T,List).

rem_dupl([],[]).
rem_dupl([H|T],Lo):-
	memberchk(H,T),!,rem_dupl(T,Lo).
rem_dupl([H|T],[H|Lo]):- rem_dupl(T,Lo).

map(_,[],[]).
map(P,[Hi|Ti],[Ho|To]):-
	P=..PL,
	append(PL,[Hi,Ho],PLfull),
	Pfull=..PLfull,
	call(Pfull),
	map(P,Ti,To).

% myfoldl(P,Init,L,Out)
% P is a predicate with (at least) 3 arguments.
% The last three are: 1 and 2: two elements to be combined
% last: the output (it is a function of the previous 2).
myfoldl(_,X,[],X).
myfoldl(P,Init,[H|T],Out):-
	P =.. LP,
	append(LP,[Init,H,Acc],LPfull),
	Pfull =.. LPfull,
	call(Pfull),
	myfoldl(P,Acc,T,Out).

viol_culprit(viol(e(_,T)),C):- arg(1,T,C).
viol_culprit(viol(en(_,T)),C):- arg(1,T,C).

multi_intersection([H|LoL],Intersection):-
	myfoldl(intersect,H,LoL,Intersection).

intersect([],_,[]).
intersect([H|T],L,Lo):- memberchk(H,L),!, Lo=[H|To], intersect(T,L,To).
intersect([_|T],L,To):- intersect(T,L,To).

% Questo predicato e` per verificare la proprieta` delle aste combinatorie:
% "Esiste un insieme di bid t.c. io ottengo uno solo fra i due elementi 1 e 2?"
% E` da raffinare, questo e` solo uno sketch di come si potrebbe fare.
iter_gsciff_auction:-
    statistics(runtime,_),
    load_ics,
    society_goal,
    N in 0..1000,
    indomain(N), write('************************* new happen ******************************'),
        writeln(N),
    add_history_el(N),
    \+((
        e(deliver(_Item1)), e(deliver(_Item2))
      )),
    close_history,
    statistics(runtime,[_,Time]),
    writeln(runtime(Time)).

% Qual e` la bid che devo fare per vincere, sapendo che
% 1. voglio minimizzare la cifra che pago
% 2. ipotizzo che l'altro faccia una bid di 5
% 3. io voglio pagare al massimo 10
best_bid(Bid):-
    statistics(runtime,_),
    load_ics,
    history,
    Price in 0..10,
            existsf(Bid), %existsf(Tbid),
            e(tell(i,auc,bid(i1,Bid),auction1)),
            existsf(Price), %existsf(Tpay),
            e(tell(i,auc,pay(Price),auction1)),
            add_history_el(2), make_choice,
    minimize(
        (   % Metto esplicitamente il society goal, perche' altrimenti
            % non posso accedere alle variabili

            indomain(Price),
            close_history, write(Price), nl
        )
        ,Price),
    statistics(runtime,[_,Time]),
    writeln(runtime(Time)).

% Explanation in help.pl
min_viol_closed(Num,OutList):-
    max_viol(Num),
    minimize(
        (   load_ics,
            society_goal,
            history,
            close_history,
            indomain(Num),
            findall_constraints(_,OutList)
        ),Num).


% Explanation in help.pl
min_viol_open(Num,OutList):-
    max_viol(Num),
    minimize(
        (   load_ics,
            society_goal,
            history,
            indomain(Num),
            findall_constraints(_,OutList)
        ),Num).

get_ics_quantified(PSICs):-
    findall(ic(Head,Body),
        ics(Head,Body),
        ICSs),
    convert_to_psic_list(ICSs,PSICs).

convert_to_psic_list([],[]).
convert_to_psic_list([IC|ICs],[PSIC|PSICs]):-
    convert_to_psic(IC,PSIC),
    convert_to_psic_list(ICs,PSICs).

allows(Param):-
    (var(Param) -> Param = false ; true), % Default behaviour: fail if not conformant
    % load_ics, I re-write the impose_ics because I don't want to redo it for
    %           each possible history (since it is expensive), so I redo only
    %           the necessary parts
    get_ics_quantified(PSICs),
    call_list(PSICs),
    %current_time(0),
    gen_phase,  % activates rationality
    history,
    society_goal,
    phase(deterministic),
    end_gen_phase,  % deactivates rationality, backtrackable
    (Param = verbose -> nl, writeln_debug('Trying history'),
    										write_history, write_normal_abducibles,
    										write_positive_expectations,
    										write_violations,
    										write_events_not_expected
        ;   true),
    (remove_exp,
     society_goal, % reactivate ics, doing a SCIFF (no g-SCIFF) computation
     %load_ics,
     %phase(deterministic), MG: Adding this (together with remove_phase) performs
                % stronger reasoning. However, in some instances
                % it can slow down very much the computation.
                % Better perform stronger reasoning in the early stages (generate)
                % and faster in other stages. Remember that in the first stages
                % we look for all solutions (where deterministic checking is
                % useful), while in the second we look for one solution
     call_list(PSICs),
     
     close_history
     -> fail   % causes backtracking
      ; writeln_debug('AlLoWS failed: no fulfilment for possible history'),nl,
        (get_option(sciff_debug, on) -> write_history,write_normal_abducibles ; true), 
        !, Param=true % deep failure
    ).
allows(_):-
    writeln_debug('AlLoWS succeded: all possible histories compliant'),
    (get_option(allow_events_not_expected, yes)
      ->    writeln_debug('Feeble conformance proven')
      ;     writeln_debug('Strong conformance proven')
    ).


remove_exp_pending @ remove_exp \ pending(_) <=> true.
remove_exp_e  @ remove_exp \ e(_,_) <=> true.
remove_exp_en @ remove_exp \ en(_,_) <=> true.
remove_exp_fulf @ remove_exp \ fulf(_) <=> true.
remove_exp_viol @ remove_exp \ viol(_) <=> true.
remove_exp_ic @ remove_exp \ ic(_,_,_,_) <=> true.
remove_exp_psic @ remove_exp \ psic(_,_,_) <=> true.
%remove_exp_en @ remove_exp \ not_expected(_) <=> true.
end_remove_exp @ remove_exp <=> true.

%iter_gsciff_vickrey:-
%    statistics(runtime,_),
%    load_ics,
%    society_goal,
%    N in 0..1000,
%    indomain(N), write('************************* new happen ******************************'),
%        writeln(N),
%    add_history_el(N),
%    close_history,
%    statistics(runtime,[_,Time]),
%    writeln(runtime(Time)).

add_history_el:-
    existsf(X), %existsf(T),
    h(X).
add_history_el:-
    existsf(X), %existsf(T),
    h(X), writeln('************************* new happen ******************************'),
    add_history_el.

add_history_el(0):- !.
add_history_el(N):-
    existsf(X),% existsf(T),
    h(X),
    N1 is N-1,
    add_history_el(N1).

% generates N happened events ordered in time
add_history_el_sym(0):- !.
add_history_el_sym(N):-
    existsf(X), %existsf(T),
    h(X), %T in 0..1000, T #=< T1,
    N1 is N-1,
    add_history_el_sym(N1).


% generates N happened events ordered in time, with a functor/arity
% (new version of SCIFF does not let one have H(A,B) with A variable
% A must be nonvar)
add_history_el_sym(0,_,_):- !.
add_history_el_sym(N,F/Arity):-
    %existsf(T),
	functor(Term,F,Arity),
	set_term_quantification(Term,existsf),
    h(Term), %T in 0..1000, T #=< T1,
    N1 is N-1,
    add_history_el_sym(N1,F/Arity).

proof:-
    init_graph('proof.dot',_Stream),
    statistics(runtime,_),
    load_ics,
    society_goal,
    history,
    once((
        write('grounding time\n'),  
        ground_time,
        write('grounding 1\n'),
        make_choice,
        true
        )),
    statistics(runtime,[_,Time]),
    writeln(runtime(Time)).


test(Pred,Mem,Time):-
    statistics(walltime,[T1,_]),
    call_always_success(Pred),
    statistics(walltime,[T2,_]),
    Time is T2 - T1,
    statistics(memory,[Mem,_]).
  
call_always_success(Pred):-
    call(Pred),
    !.
call_always_success(_).




clp_constraint(A):-
    call(A).


:- chr_constraint
	cug/1.
cug(X) <=> ground(X)|call(X).




/*
constraints novis/0.

firstfail @
    e(X1,T1), e(X2,T2), ground_time
    ==> fd_var(T1), fd_var(T2), fd_size(T1,S1), fd_size(T2,S2), S1<S2 |
        write(size(S1)),
        indomain(T1).

novisual @ ground_time, novis,
    e(X,T) ==> fd_var(T) | T in -1000..1000, indomain(T).      

visual @ e(X,T), ground_time
    ==> fd_var(T) |  T in -1000..1000,
    fd_min(T,Min),fd_max(T,Max), write(T in Min..Max), indomain(T), nl,
write(T), novis.
*/

:- chr_constraint all_e/1, get_list_e/1.

e(F,X) \ all_e(L) <=> notmember(e(F,X),L) | all_e([e(F,X)|L]).

all_e(L1), get_list_e(L2) <=> L1=L2.

findall_e(L):-
    all_e([]), get_list_e(L), !.

notmember(_,[]).
notmember(A,[H|_]):- A==H, !, fail.
notmember(A,[_|T]):- notmember(A,T).

% MG 9 Feb 2008: I don't use the findall_constraints implemented in my_chr_extensions,
% because it does not retain the variables (it is due to the implementation of
% findall in SICStus)
ground_time:- 
    findall_e(L),
    get_time(L,LT),
    solver_search(LT).

get_time([],[]).
get_time([e(_,_)|R],[T|RT]):-
    add_default_domain(T),
    get_time(R,RT).
    
ground_abd:-
    findall_constraints_nsquare(abd(_F,_X),L),
    term_variables(L,Lvar),
    add_default_domain_list(Lvar),
    solver_search(Lvar).

add_default_domain_list([]).
add_default_domain_list([H|T]):-
    add_default_domain(H),
    add_default_domain_list(T).

build_and_run(SOKB, ICS, History, noclose) :-
	build(SOKB, ICS, History),
	consult(sokb),
	use_module(history),
	use_module(ics),
	run_no_close.

build_and_run(SOKB, ICS, History, close) :-
	build(SOKB, ICS, History),
	consult(sokb),
	use_module(history),
	use_module(ics),
	run.

project(Project):-
   default_dir(Dir),
   atom_concat(Dir,Project,Path),
   atom_concat(Path,'/',PathSlash),
   atom_concat(PathSlash,'project.pl',PrjFile),
   compile(PrjFile),
   build_prj(PathSlash).

append_path(_,[],[]):- !.
% If the file is actually a URL, do not change it
append_path(Path,[File|Rest],[FullPath|Rest1]):-
    atom_concat('http://',_,File),!,FullPath=File,
    append_path(Path,Rest,Rest1).
append_path(Path,[File|Rest],[FullPath|Rest1]):-
    atom_concat(Path,File,FullPath),
    append_path(Path,Rest,Rest1).

convert_sokb([],_).
convert_sokb([SOKB|Rest],File):-
    write_debug('Parsing file '), write_debug(SOKB),
    translate_sokb(SOKB,File,write), % for the 1st, write, the others are in append
    writeln_debug(' --> OK'),
    convert_sokb1(Rest,File).
convert_sokb1([],_).
convert_sokb1([SOKB|Rest],File):-
    write_debug('Parsing file '), write_debug(SOKB),
    translate_sokb(SOKB,File,append),
    writeln_debug(' --> OK'),
    convert_sokb1(Rest,File).
    
/*
build(SOKB,ICS,History):-
   translate_sokb(SOKB,'./sokb.pl'),
    translate_ics(ICS,'./ics.pl'),
    translate_history(History,'./history.pl').

build(ExampleName):-
    atom_concat(ExampleName,'/',A1),
    atom_concat(A1,ExampleName,A2),
    atom_concat(A2,'_sokb.pl',SOKB),
    atom_concat(A2,'_ics.txt',ICS),
    atom_concat(A2,'_history.txt',History),
    build(SOKB,ICS,History).
*/

% Discharges the expectation by making it fulfilled.
% See Alberti et al @ Jurisin 2016

discharge(e(Event)):-
    get_functor(Event,F),
    fulf(e(F,Event)),
    % CILC2017->FI:
    % I add a new abducible "discharged", because in the case of nested discharging I need to know
    % if an atom was explicitly discharged
    abd(discharged(e(Event)),0).
    
    
discharge(en(Event)):-
    get_functor(Event,F),
    fulf(en(F,Event)),
    abd(discharged(en(Event)),0).


% I add a new abducible "discharged", because in the case of nested discharging I need to know
% if an atom was explicitly discharged



% -----------------------------------
% PROBABILITY COMPUTATION
% -----------------------------------
% TODO merge tornado
:- thread_local
        rule_n/1,
        na/2,
        v/3.

get_bdd_environment(Env):-
    init(Env).

clean_environment(Env):-
    end(Env).

/**
 * compute_prob(,+Lworlds:list,--Prob:float)
 * 
 * Takes as input the list of worlds Lworlds and returns the corresponding probability Prob.
 */
compute_prob(Lworlds,Prob):-
    retractall(v(_,_,_)),
    retractall(na(_,_)),
    retractall(rule_n(_)),
    assert(rule_n(0)),
    get_bdd_environment(Env),
    build_bdd(Env,Lworlds,BDD),
    ret_prob(Env,BDD,Prob),
    get_symbolic_equation(Env,BDD,SEq), writeln(SEq),
    %create_dot_string(Env,BDD,SDOT),writeln(SDOT),
    clean_environment(Env), !.
/*
compute_prob(Lworlds,Prob):-
    compute_prob_int(Lworlds,0,Prob),!.

compute_prob_int([],Prob,Prob):-!.

compute_prob_int([world(_ICS,NUC,ProbWorld)|T],ProbT0,Prob):-
    skolemize(NUC),
    ProbT is ProbT0 + ProbWorld,
    compute_prob_int(T,ProbT,Prob).
*/

get_symbolic_equation(Env,BDD,SEq):-
  findall([I,N,P],(opt(I,N,P),\+number(N)),LIndexNameProb),
  ret_equation_bdd_c(Env,(_,BDD),LIndexNameProb,SEq0),
  evaluate_expr(SEq0,LIndexNameProb,SEq).

evaluate_expr(Formula,VariablesList):-
  evaluate_expr(Formula,VariablesList,Result),
  writeln(Result).

evaluate_expr(Formula,VariablesList,ToEvaluate):-
  % TODO: check that all the variables are in the list
  % TODO: check that the list is well formed
  ( string(Formula) ->
    term_string(T_Formula, Formula) ;
    T_Formula = Formula
  ),
  ( string(VariablesList) ->
    term_string(VL, VariablesList) ;
    VL = VariablesList
  ),
  replace_vars(T_Formula,VL,ToEvaluate).


replace_vars(Res,[],Res):- !.
replace_vars(Res,[[_,Find,Replace]|T],Result):-
  replace(Find,Replace,Res,Res0),
  replace_vars(Res0,T,Result).

replace(TermFind, TermReplace, Compound, CompOut) :-
  ( var(Compound) ->
    CompOut = Compound ;
    ( Compound == TermFind ->
      CompOut = TermReplace ;
        Compound =.. [F|Args0],
        maplist(replace(TermFind,TermReplace), Args0, Args),
        replace_op(F,FN),
        CompOut =.. [FN|Args]
    )
  ).

% evaluate_expr("0*1.0000+2*1*(1-0)*1.0000+3*2*(1-1)*(1-0)*1.0000",[[0,A],[1,B],[2,C],[3,0.2]]).

replace_op(OP,OP):-
  number(OP),!.
replace_op(OP,OPN):-
  OPN = oprt{'+':add, '-':subt, '*':mul, '/':divd}.get(OP),!.
replace_op(OP,OP).

/**
 * build_bdd(+Env:atom,+Lworlds:list,--BDD:atom)
 * 
 * Takes as input the BDD environment Env and a list of worlds Lworlds and returns the corresponding BDD.
 */
build_bdd(Env,[],BDD):- !,
    zero(Env,BDD).

% ICS: list of IC, NUC: list of not_unify_constr, Prob: probability
build_bdd(Env,[world(ICS,NUC,_Prob)],BDD):- !,
    %build_bdd(Env,[world(ICS,_Prob)],BDD):- !,
    skolemize(NUC),
    bdd_and(Env,ICS,NUC,BDD),
    get_symbolic_equation(Env,BDD,SEq),writeln(SEq).
    %bdd_and(Env,ICS,test,BDD).

build_bdd(Env, [world(ICS,NUC,_Prob)|T],BDD):-
%build_bdd(Env, [world(ICS,_Prob)|T],BDD):-
    build_bdd(Env,T,BDDT),
    skolemize(NUC),
    bdd_and(Env,ICS,NUC,BDDH),
    get_symbolic_equation(Env,BDDH,SEq),writeln(SEq),
    %bdd_and(Env,ICS,test,BDDH),
    or(Env,BDDH,BDDT,BDD).
    %ret_prob(Env,BDDH,PH),
    %ret_prob(Env,BDDT,PT),
    %ret_prob(Env,BDD,POr),
    %format("~f - ~f - ~f~n",[PH,PT,POr]).

/*
world(
    [
    ics(removed,[abd(p(_168198),1)],[[abd(q(_168198,_169258),1)]],1,0.4),
    ics(added,[abd(p(_168758),1)],[[abd(q(_168758,_168910),1)]],1,0.4)
    ],
    
    [not_unify_constr(_168198,_168758),not_unify_constr(_168758,_168198)],
    
    0.24).
*/

bdd_and(Env,[X],NUC,BDDX):-
    get_prob_ax(X,NUC,AxN,Neg,Prob),!,
    ProbN is 1-Prob,
    get_var_n(Env,AxN,[],[Prob,ProbN],VX),
    get_var_bdd(Env,VX,0,Neg,BDDX),!.

bdd_and(Env,[_X],_NUC,BDDX):- !,
    one(Env,BDDX).

bdd_and(Env,[H|T],NUC,BDDAnd):-
    get_prob_ax(H,NUC,AxN,Neg,Prob),!,
    ProbN is 1-Prob,
    get_var_n(Env,AxN,[],[Prob,ProbN],VH),
    get_var_bdd(Env,VH,0,Neg,BDDH),
    bdd_and(Env,T,NUC,BDDT),
    and(Env,BDDH,BDDT,BDDAnd).
    %ret_prob(Env,BDDAnd,PAnd),writeln(PAnd).
  
bdd_and(Env,[_H|T],NUC,BDDAnd):- !,
    one(Env,BDDH),
    bdd_and(Env,T,NUC,BDDT),
    and(Env,BDDH,BDDT,BDDAnd).



/* Utilities */
% Returns the variable corresponding with axiom R having the probabilities in Prob.
% If it does not exist it assings a new variable to axiom
get_var_n(Env,R,S,Probs,V):-
    (
      v(R,S,V) ->
        true
      ;
        %length(Probs,L),
        add_opt_var(Env,Probs,R,V),
        assert(v(R,S,V))
    ).
  

get_prob_ax(ics(AOR,H,B,ICID,Prob),_NUC,N,Neg,Prob):- !,
    compute_prob_IC(ics(AOR,H,B,ICID,Prob),Neg,Prob),
    ( na(ics(_,H,B,ICID,Prob),N) ->
        true
        ;
        rule_n(N),
        assert(na(ics(_,H,B,ICID,Prob),N)),
        ( number(Prob) -> term_string(Prob,ProbS) ; term_string(Prob,ProbS) ),
        assert(opt(N,ProbS,Prob)),
        retract(rule_n(N)),
        N1 is N + 1,
        assert(rule_n(N1))
    ).

% ROA: removed or added
% ICID: ID of the IC

% qua tratto ic removed o added come lo stesso IC -> stessa variabile. Nego il BDD nel caso di removed
compute_prob_IC(ics(added,_H,_B,_ICID,Prob),1,Prob):-!.

compute_prob_IC(ics(removed,_H,_B,_ICID,Prob),0,Prob):-!.

get_var_bdd(Env,VX,0,1,BDDX):- !,
    equality(Env,VX,0,BDDX).

get_var_bdd(Env,VX,0,0,BDDX):- !,
    equality(Env,VX,0,BDDXN),
    bdd_not(Env,BDDXN,BDDX).

/*
% qua tratto ic removed o added come diversi IC -> diverse variabili. Non nego il BDD nel caso di removed
compute_prob_IC(ics(added,_H,_B,_ICID,Prob),1,Prob):-!.

compute_prob_IC(ics(removed,_H,_B,_ICID,Prob),0,NProb):-!,
    NProb is 1-Prob.

get_var_bdd(Env,VX,0,_,BDDX):- !,
  equality(Env,VX,0,BDDX).
*/

skolemize(L):-
    skolemize_(L,0).

skolemize_([],_):-!.

skolemize_([not_unify_constr(V0,V1)|T],C):-
    (
        ground(V0) -> (C1 is C) ; 
        (
            get_skolem_var_name(C,V0),
            C1 is C + 1
        )
    ),
    (
        ground(V1) -> (CN is C1) ;
        (
            get_skolem_var_name(C1,V1),
            CN is C1 + 1
        )
    ),
    skolemize_(T,CN). 

get_skolem_var_name(C,V):-
    atom_concat('Var',C,V).

check_worlds(LW0,LW):-
    check_worlds_int(LW0,LW1),
    remove_worlds_subsets(LW1,LW).


check_worlds_int([],[]).
check_worlds_int([H|T],TP):-
    check_this_world(H,T),!,
    check_worlds_int(T,TP).

check_worlds_int([H|T],[H|TP]):-
    check_worlds_int(T,TP).

check_this_world(H,T):-
    check_worlds_copies(H,T),!.

%MG: As it is, the following clause is redundant.
% maybe swap the cut and the fail?
check_worlds_copies(_,[]):-fail,!.

check_worlds_copies(world(ExplW,_,_),[world(ExplH,_,_)|_]):-
    length(ExplW,NE),
	length(ExplH,NE),
    sort(ExplH,ExplSort),
    sort(ExplW,ExplSort),!,write('found_copy!'),nl.

check_worlds_copies(world(ExplW,_,ProbW),[_|T]):-
    check_worlds_copies(world(ExplW,_,ProbW),T).


remove_worlds_subsets(LW0,LW):- %gtrace,
    remove_worlds_subsets(LW0,LW0,LW).

remove_worlds_subsets([],LW,LW).

remove_worlds_subsets([world(ExplW,_,_)|T],LW0,LW):-
    remove_worlds_subsets_int(world(ExplW,_,_),LW0,LW1),
    remove_worlds_subsets(T,LW1,LW).

remove_worlds_subsets_int(world(ExplW,_,_),LW0,LW):-
    member(world(ExplH,A,P),LW0),
    dif(ExplW,ExplH),
    subset(ExplW,ExplH),!,write('found_subset!'),nl,
    delete(LW0,world(ExplH,A,P),LW1),
    remove_worlds_subsets_int(world(ExplW,_,_),LW1,LW).

remove_worlds_subsets_int(_,LW,LW).
